using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;


using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using Ducserver;
using Ducproto;

namespace Ducserver
{

    /// <summary>
    /// A Syntax Tree walker used to construct the DU Chain.
    /// </summary>
    class DUBuilder : CSharpSyntaxWalker
    {
        /// The root of the constructed DU Chain (excluding imports)
        public readonly TreeNode root = new TreeNode();

        /// A semantic model used to get information about symbols during visiting
        public readonly SemanticModel model;

        /// A stack of nodes used to build the DU Chain;

        /// The Node that is currently being built is on the top of the stack
        ///
        /// When one creates a new node (e.g. when visiting a class declaration)
        /// the new node is first added as a child of the node at the top of the
        /// stack. It is then pushed onto the stack and its children are recursively
        /// built during visiting the syntax subtree (e.g. members of the class);
        /// finally, once visiting the subtree is finished, the node is removed
        /// from the top of the stack (since it already is a child of its parent,
        /// and since we remember the root, it is not lost)
        private readonly Stack<TreeNode> nodeStack = new Stack<TreeNode>();

        /// A dictionary mapping file-names to respective ImportNodes
        private readonly Dictionary<String,TreeNode> imports = new Dictionary<String,TreeNode>();


        private void PrintIndented(params string[] args) {
          Console.WriteLine(String.Join(' ', nodeStack.Select(elt => " ").Concat(args)));
        }

        /***********************************************************************
         *                  HELPER CONSTRUCTING FUNCTIONS                      *
         ***********************************************************************/

        /// Constructs and populates an instance of a Range from either a SyntaxNode or
        /// a SyntaxToken
        private Range GetNodeRange(dynamic node) {
            if (node is null)
                return new Range() {
                    Start = new Ducproto.Location(),
                    End = new Ducproto.Location()
                };
            var c = node.GetLocation();
            var start = node.GetLocation().GetLineSpan().StartLinePosition;
            var end = node.GetLocation().GetLineSpan().EndLinePosition;
            return new Range() {
                    Start = new Ducproto.Location()
                    {
                        Line = start.Line,
                        Column = start.Character
                    },
                    End = new Ducproto.Location()
                    {
                        Line = end.Line,
                        Column = end.Character
                    },
            };
        }

        /// Constructs and populates an instance of a LocatedName which corresponds to the
        /// source-range containing the name-part of the SyntaxNode/SyntaxToken node
        /// (e.g. given a method
        /// 31
        /// 32    private void ExampleMethod(int param) {
        /// 33
        /// ...
        /// 40    }
        /// )
        /// The LocatedName for the SyntaxNode corresponding to the method declaration
        /// would return (32,17)--(32,30), ExampleMethod
        private LocatedName GetLocatedName(dynamic node) {
            return new LocatedName() {
                Name = node.ToString(),
                Location = GetNodeRange(node)
            };
        }

        // Creates a new DU Node, populating its location based on the location of @p node
        // and type from @p type. Depending on the type, it also populates the appropriate
        // *Data field.
        private TreeNode GetDUNode(SyntaxNode node, TreeNode.Types.NodeType type = TreeNode.Types.NodeType.Context)
        {
                var du_node = new TreeNode();
                du_node.Type = type;
                du_node.SourceLocation = GetNodeRange(node);
                switch(type) {
                    case TreeNode.Types.NodeType.Context:
                        du_node.CtxData = new Context();
                        break;
                    case TreeNode.Types.NodeType.Use:
                        du_node.UseData = new Use();
                        break;
                    case TreeNode.Types.NodeType.Import:
                        du_node.ImportData = new Import();
                        break;
                    case TreeNode.Types.NodeType.Declaration:
                        du_node.DeclData = new Declaration();
                        break;
                    default:
                        break;
                }
                return du_node;
        }

        private String GetComments(SyntaxNode node) {
            return (String.Join(" ", node.GetLeadingTrivia().Select(
                t => t.ToString().Trim(new[]{'/', ' ', '\t', '\n'})
            )) + String.Join(" ", node.GetTrailingTrivia().Select(
                t => t.ToString().Trim(new[]{'/', ' ', '\t', '\n'})
            ))).Trim();
        }


        /// Returns the source file, identifier uid and its fully qualified name (as a list of strings),
        /// if available
        protected (string, string, string[]) GetIdentifierName(SyntaxNode node, ISymbol symbol = null) {
                if (node is null)
                    return ("", "", new string[] {});

                if (symbol is null) {
                    var symbolInfo = model.GetSymbolInfo(node);
                    symbol = symbolInfo.Symbol ?? (symbolInfo.CandidateSymbols.Length > 0 ? symbolInfo.CandidateSymbols[0] : null);
                }

                // If there is no semantic info, just convert the node to string
                // and call it a day
                if (symbol is null)
                    return (model.SyntaxTree.FilePath, node.ToString(), node.ToString().Split("."));

                // Get the source file where the symbol is declared
                // and add an appropriate import node
                var sourceFile = symbol.Locations.Length > 0 ?
                                    symbol.Locations[0].SourceTree?.FilePath??"" :
                                    "";

                var uid = symbol.ToString();
                var nameSpace = "";
                if (symbol.Kind == SymbolKind.Namespace)
                    nameSpace = symbol.Name.ToString();
                else
                    nameSpace = symbol.ContainingNamespace?.ToString() ?? "";

                // Construct the qualified name
                List<string> qualifiedName = new List<string>();
                while(symbol != null && symbol.Kind != SymbolKind.Namespace) {
                    var simpleName = symbol.CanBeReferencedByName ? symbol.Name.ToString().Split('.').Last() : symbol.ToString();
                    if (simpleName.Length > 0)
                        qualifiedName.Add(simpleName);
                    symbol = symbol.ContainingSymbol;
                }

                if (nameSpace.Length > 0)
                    qualifiedName.Add(nameSpace);
                qualifiedName.Reverse();

                // If the uid does not contain a dot, it is probably a simple variable name
                // and therefore not unique, so we add the names of the containing elements
                // to try to make it unique
                if (! uid.Contains(".") )
                    uid = String.Join(".", qualifiedName);

                return (sourceFile, uid, qualifiedName.ToArray());
        }

        /***********************************************************************
         *                       HELPER VISITOR FUNCTIONS                      *
         ***********************************************************************/

        /// Creates a new context node, adds it to the current DUNode as a child
        /// and pushes it onto the node stack.
        protected void OpenContext(SyntaxNode node, Context.Types.ContextType ctx_type)
        {
            var ctx = GetDUNode(node);
            ctx.CtxData = new Context()
            {
                Kind = ctx_type,
            };
            nodeStack.Peek().Children.Add(ctx);
            nodeStack.Push(ctx);
        }
        /// Finishes constructing the current context
        /// (i.e. removes the top element from the node stack)
        protected void CloseContext()
        {
            nodeStack.Pop();
        }

        /// Determines whether the node @p node needs a special context
        /// (i.e. a code-block requires a new context, since variables
        ///  declared inside are not visible outside) and, if so, returns
        /// its type. Otherwise returns null;
        /// <note>
        /// Class/Method Declarations open contexts on their own, so NodeToContextType
        /// returns null for them.
        /// </note>
        protected Context.Types.ContextType? NodeToContextType(SyntaxNode node)
        {
            if (node is BlockSyntax)
                return Context.Types.ContextType.Other;
            return null;
        }

        /// Creates a new Declaration Node, adds it to the current node as a child,
        /// pushes it onto the stack and returns it.
        /// <note>
        /// The method also populates the Kind, DeclUid and Comment fields
        /// of the node's DeclData member
        /// </note>
        protected TreeNode OpenDeclaration(SyntaxNode node, Declaration.Types.DeclarationType declType) {
            var declNode = GetDUNode(node, TreeNode.Types.NodeType.Declaration);
            var (_,  uid,  _) = GetIdentifierName(node, model.GetDeclaredSymbol(node));
            declNode.Type = TreeNode.Types.NodeType.Declaration;
            declNode.DeclData = new Declaration() {
                Kind = declType,
                DeclUid = uid,
                Comment = GetComments(node),
            };
            PrintIndented("Opening declaration:", declNode.DeclData.DeclUid);
            nodeStack.Peek().Children.Add(declNode);
            nodeStack.Push(declNode);
            return declNode;
        }

        /// Finishes constructing the current declaration
        /// (i.e. removes the top element from the node stack)
        protected void CloseDeclaration()
        {
            nodeStack.Pop();
        }

        /// Adds an Use node corresponding to the syntax node @p node
        /// to the DUChain
        protected void UseNode(SyntaxNode node) {
            var (sourceFile,  uid,  qualifiedName) = GetIdentifierName(node, model.GetDeclaredSymbol(node));
            if (qualifiedName.Length > 0)
                AddUse(sourceFile, uid, qualifiedName, GetNodeRange(node));
        }

        /// Adds a use of the qualified name @p qualifiedName
        /// declared in @p sourceFile with unique id @p uid
        /// at the location @p location to the DU Chain
        protected void AddUse(string sourceFile, string uid, string[] qualifiedName, Range location) {
            var use_node = new TreeNode() {
                Type = TreeNode.Types.NodeType.Use,
                SourceLocation = location,
                UseData = new Use() {
                    DeclUid = uid
                }
            };
            use_node.UseData.QualifiedName.Add(qualifiedName);
            nodeStack.Peek().Children.Add(use_node);
            if (sourceFile.Length > 0)
                AddImport(sourceFile);
        }

        /// Records a file import (e.g.example Using ...) of @p path
        public void AddImport(string path) {
            if (path is null || path == model.SyntaxTree.FilePath || imports.ContainsKey(path)) return;
            imports[path] = new TreeNode() {
                Type = TreeNode.Types.NodeType.Import,
                ImportData = new Import() {
                    Path = path
                }
            };
        }

        /***********************************************************************
         *                       TYPE SYSTEM HELPERS                           *
         ***********************************************************************/

        /// Converts a builtin C# type to a corresponding DUChain type
        public BuiltinTypeData.Types.BuiltInType? GetBuiltinType(String TypeName) {
            switch(TypeName) {
                    case "Int32":
                    case "int":
                        return BuiltinTypeData.Types.BuiltInType.Int;
                    case "long":
                    case "ulong":
                        return BuiltinTypeData.Types.BuiltInType.Long;
                    case "char": return BuiltinTypeData.Types.BuiltInType.Char;
                    case "Boolean":
                    case "bool":
                        return BuiltinTypeData.Types.BuiltInType.Boolean;
                    case "float": return BuiltinTypeData.Types.BuiltInType.Float;
                    case "double": return BuiltinTypeData.Types.BuiltInType.Double;
                    case "string":
                    case "String":
                        return BuiltinTypeData.Types.BuiltInType.String;
                    case "void":
                    case "Void":
                        return BuiltinTypeData.Types.BuiltInType.Void;
                    case "dynamic":
                        return BuiltinTypeData.Types.BuiltInType.Mixed;
                    default: return null;
                }
        }
        /// Constructs a TypeData structure populating it with data from
        /// a SyntaxNode or a SyntaxToken @p element refering to a type
        public TypeData GetTypeData(String TypeName, dynamic element) {
            var data = new TypeData() {
                Name = new LocatedName() {
                    Name = TypeName,
                    Location = GetNodeRange(element),
                }
            };
            var builtin = GetBuiltinType(TypeName);
            if (builtin is null) {
                data.Kind = TypeData.Types.TypeKind.Structure;
            } else {
                data.Kind = TypeData.Types.TypeKind.Builtin;
                data.BuiltinData = new BuiltinTypeData() {
                    Kind = (BuiltinTypeData.Types.BuiltInType) builtin
                };
            }
            return data;
        }

        /// Constructs a TypeData structure populating it with data from
        /// a SyntaxNode or a SyntaxToken @p element refering to a type.
        /// Returns the qualified name of the type together with this structure
        protected TypeData GetInstanceTypeData(dynamic instanceNode) {
            if (instanceNode.Type == null)
                return GetTypeData("var", null);
            var hasImplicitType = (instanceNode.Type.ToString() == "var");
            var (_,  _,  qualifiedTypeName) = GetIdentifierName((SyntaxNode)instanceNode.Type);
            return GetTypeData(
                        hasImplicitType ?
                            qualifiedTypeName.Last() :
                            instanceNode.Type.ToString(),
                        instanceNode.Type);
        }


        /***********************************************************************
         *                             VISITORS                                *
         ***********************************************************************/

        /// Basic SyntaxNode visitor, called for every visited node in the Syntax Tree.
        /// Takes care of opening/closing contexts if necessary.
        /// Also catches exceptions in other visitors and ignores them
        public override void Visit(SyntaxNode node)
        {
            var ctx_type = NodeToContextType(node);
            if (ctx_type.HasValue) OpenContext(node, ctx_type.Value);
            try {
                base.Visit(node);
            } catch (Exception e) {
                PrintIndented("Encountered exception", e.Message.ToString(), "at", e.Source.ToString());
            }
            if (ctx_type.HasValue) CloseContext();
        }

        /* STRUCTURE DECLARATIONS */

        /// Sets Class/Struct/Interface/Namespace modifiers (e.g. public/private/static, ...)
        protected void setMemberModifiers(SyntaxTokenList? modifiers, Declaration data) {
            if (modifiers.HasValue)
                foreach(var prop in modifiers) {
                    switch(prop.Kind()) {
                        case SyntaxKind.AbstractKeyword:
                            data.MethodData.Abstract = true;
                            break;
                        case SyntaxKind.PublicKeyword:
                            data.Access = AccessPolicy.Public;
                            break;
                        case SyntaxKind.PrivateKeyword:
                            data.Access = AccessPolicy.Private;
                            break;
                        case SyntaxKind.ProtectedKeyword:
                            data.Access = AccessPolicy.Protected;
                            break;
                        case SyntaxKind.StaticKeyword:
                            data.MethodData.MemberSpec.Static = true;
                            break;
                        default:
                            break;
                    }
                };

        }
        /// Handles Interface Declarations
        /// <note>
        /// interface declarations are represented via class declarations in the DU Chain, since
        /// it does not have a separate declaration type for interfaces
        /// </note>
        public override void VisitInterfaceDeclaration(InterfaceDeclarationSyntax node)
        {
            PrintIndented("Visiting Interface Declaration", node.Identifier.ToString());
            var class_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Class);
            class_node.DeclData.Name = GetLocatedName(node.Identifier);
            class_node.DeclData.ClassData = new ClassDeclData () {
                Kind = ClassDeclData.Types.ClassType.Interface,
                Modifier = ClassDeclData.Types.ClassModifier.None,
            };
            base.VisitInterfaceDeclaration(node);
            CloseDeclaration();
        }
        public override void VisitStructDeclaration(StructDeclarationSyntax node)
        {
            PrintIndented("Visiting Struct Declaration", node.Identifier.ToString());
            var class_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Class);
            class_node.DeclData.Name = GetLocatedName(node.Identifier);
            class_node.DeclData.ClassData = new ClassDeclData () {
                    Kind = ClassDeclData.Types.ClassType.Struct,
                    Modifier = ClassDeclData.Types.ClassModifier.Final,
            };

            // Handle abstract modifiers
            if (node.Modifiers != null)
                foreach(var mod in node.Modifiers) {
                    if (mod.Kind() == SyntaxKind.AbstractKeyword) {
                        class_node.DeclData.ClassData.Modifier = ClassDeclData.Types.ClassModifier.Abstract;
                        break;
                    }
                }

            // Handle base classes
            if (node.BaseList != null)
                foreach(var base_class in node.BaseList.Types) {
                    class_node.DeclData.ClassData.BaseClasses.Add(new AncestorClass() {
                        Name = GetLocatedName(base_class),
                        Access = AccessPolicy.DefaultAccess,
                        Virtual = false
                    });
                    UseNode(base_class.Type);
                }
            base.VisitStructDeclaration(node);
            CloseDeclaration();
        }
        public override void VisitClassDeclaration(ClassDeclarationSyntax node)
        {
            PrintIndented("Visiting Class Declaration", node.Identifier.ToString());
            var class_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Class);
            class_node.DeclData.Name = GetLocatedName(node.Identifier);
            class_node.DeclData.ClassData = new ClassDeclData () {
                    Kind = ClassDeclData.Types.ClassType.Class,
                    Modifier = ClassDeclData.Types.ClassModifier.None,
            };

            // Handle sealed and abstract modifiers
            if (node.Modifiers != null)
                foreach(var mod in node.Modifiers) {
                    if (mod.Kind() == SyntaxKind.SealedKeyword) {
                        class_node.DeclData.ClassData.Modifier = ClassDeclData.Types.ClassModifier.Final;
                        break;
                    }
                    if (mod.Kind() == SyntaxKind.AbstractKeyword) {
                        class_node.DeclData.ClassData.Modifier = ClassDeclData.Types.ClassModifier.Abstract;
                        break;
                    }
                }

            // Handle base classes
            if (node.BaseList != null)
                foreach(var base_class in node.BaseList.Types) {
                    class_node.DeclData.ClassData.BaseClasses.Add(new AncestorClass() {
                        Name = GetLocatedName(base_class),
                        Access = AccessPolicy.DefaultAccess,
                        Virtual = false
                    });
                    UseNode(base_class.Type);
                }
            base.VisitClassDeclaration(node);
            CloseDeclaration();
        }
        public override void VisitNamespaceDeclaration(NamespaceDeclarationSyntax node)
        {
            PrintIndented("Visiting Namespace Declaration", node.Name.ToString());
            var namespace_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Namespace);
            namespace_node.DeclData.Name = GetLocatedName(node.Name);
            base.VisitNamespaceDeclaration(node);
            CloseDeclaration();
        }


        /* FUNCTION DECLARATIONS */

        /// Processes method argument lists
        /// Adding declarations for each argument to the method data adding uses for each argument type
        protected void processArgumentList(IEnumerable<ParameterSyntax> arguments, MethodDeclData data) {
            foreach(var arg in arguments) {
                var arg_decl = GetDUNode(arg, TreeNode.Types.NodeType.Declaration);
                arg_decl.DeclData.Name = GetLocatedName(arg.Identifier);
                arg_decl.DeclData.Kind = Declaration.Types.DeclarationType.Instance;
                arg_decl.DeclData.InstanceData = new InstanceDeclData() {
                    Type = GetTypeData(arg.Type.ToString(), arg.Type),
                };
                data.FunctionSpec.Arguments.Add(arg_decl);
                UseNode(arg.Type);
            };
        }

        public override void VisitSimpleLambdaExpression(SimpleLambdaExpressionSyntax node) {
            OpenContext(node, Context.Types.ContextType.Other);

            var variable_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Instance);
            variable_node.DeclData.Name = GetLocatedName(node.Parameter.Identifier);
            variable_node.DeclData.InstanceData = new InstanceDeclData() {
                ImplicitType = ((node.Parameter.Type?.ToString()?? "var") == "var"),
                Type = GetInstanceTypeData(node.Parameter),
            };
            CloseDeclaration();

            base.VisitSimpleLambdaExpression(node);
            CloseContext();
        }

        public override void VisitDestructorDeclaration(DestructorDeclarationSyntax node) {
            PrintIndented("Visiting Destructor Declaration:", node.Identifier.ToString()+"()");
            var method_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Method);
            /* method_node.DeclData.DeclUid = model.GetDeclaredSymbol(node)?.ToString()??""; */
            method_node.DeclData.Name = GetLocatedName(node.Identifier);
            method_node.DeclData.MethodData = new MethodDeclData()
            {
                Constructor = false,
                Destructor = true,
                FunctionSpec = new FunctionDeclData() {},
                MemberSpec = new ClassMemberDeclData() {
                    Static = false,
                    Friend = false,
                    Extern = false,
                    Register = false,
                    Mutable = true,
                }
            };
            setMemberModifiers(node.Modifiers, method_node.DeclData);
            method_node.DeclData.Name = GetLocatedName(node.Identifier);
            base.VisitDestructorDeclaration(node);
            CloseDeclaration();
        }

        public override void VisitConstructorDeclaration(ConstructorDeclarationSyntax node) {
            PrintIndented("Visiting Constructor Declaration:", node.Identifier.ToString()+"()");
            var method_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Method);
/*             method_node.DeclData.DeclUid = model.GetDeclaredSymbol(node)?.ToString()??""; */
            method_node.DeclData.Name = GetLocatedName(node.Identifier);
            method_node.DeclData.MethodData = new MethodDeclData()
            {
                Constructor = true,
                Destructor = false,
                FunctionSpec = new FunctionDeclData() {},
                MemberSpec = new ClassMemberDeclData() {
                    Static = false,
                    Friend = false,
                    Extern = false,
                    Register = false,
                    Mutable = true,
                }
            };
            setMemberModifiers(node.Modifiers, method_node.DeclData);
            if (node.ParameterList?.Parameters != null)
                processArgumentList(node.ParameterList?.Parameters, method_node.DeclData.MethodData);
            method_node.DeclData.Name = GetLocatedName(node.Identifier);
            base.VisitConstructorDeclaration(node);
            CloseDeclaration();
        }
        public override void VisitMethodDeclaration(MethodDeclarationSyntax node)
        {
            PrintIndented("Visiting Method Declaration:", node.ReturnType.ToString(), node.Identifier.ToString()+"()");
            var method_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Method);
/*             method_node.DeclData.DeclUid = model.GetDeclaredSymbol(node)?.ToString()??""; */
            method_node.DeclData.Name = GetLocatedName(node.Identifier);
            method_node.DeclData.MethodData = new MethodDeclData()
            {
                Explicit = !(node.ExplicitInterfaceSpecifier is null),
                Constructor = false,
                Destructor = false,
                FunctionSpec = new FunctionDeclData() {
                    RetType = GetTypeData(node.ReturnType.ToString(), node.ReturnType)
                },
                MemberSpec = new ClassMemberDeclData() {
                    Static = false,
                    Friend = false,
                    Extern = false,
                    Register = false,
                    Mutable = true,
                }
            };
            UseNode(node.ReturnType);
            setMemberModifiers(node.Modifiers, method_node.DeclData);
            if (node.ParameterList?.Parameters != null)
                processArgumentList(node.ParameterList?.Parameters, method_node.DeclData.MethodData);
            method_node.DeclData.Name = GetLocatedName(node.Identifier);
            base.VisitMethodDeclaration(node);
            CloseDeclaration();
        }

        /* VARIABLE DECLARATIONS */

        public override void VisitFieldDeclaration(FieldDeclarationSyntax node) {
            base.VisitFieldDeclaration(node);
        }
        public override void VisitVariableDeclaration(VariableDeclarationSyntax node)
        {
            PrintIndented("Visiting Variable Declarations of type:",
                    node.Type.ToString(),
                    "("+(model.GetSymbolInfo(node.Type).Symbol?.ToString()??"")+")"
            );
            var hasImplicitType = (node.Type.ToString() == "var");
            var (sourceFile,  uid,  qualifiedTypeName) = GetIdentifierName(node.Type);
            if (node.Variables != null)
                foreach (var Variable in node.Variables)
                {
                    var variable_node = OpenDeclaration(Variable, Declaration.Types.DeclarationType.Instance);
                    variable_node.DeclData.Name = GetLocatedName(Variable.Identifier);
                    variable_node.DeclData.Comment = (GetComments(node)+"\n"+GetComments(Variable)).Trim();
                    variable_node.DeclData.InstanceData = new InstanceDeclData() {
                        ImplicitType = hasImplicitType,
                        Type = GetInstanceTypeData(node),
                    };
                    CloseDeclaration();
                }
            AddUse(sourceFile, uid, qualifiedTypeName, GetNodeRange(node.Type));
            base.VisitVariableDeclaration(node);
        }

        /// Visit a foreach statement and declare the iteration variable.
        public override void VisitForEachStatement(ForEachStatementSyntax node) {
            // Open a new context (the iteration variable is only visible inside)
            OpenContext(node, Context.Types.ContextType.Other);

            // Declare the variable
            var variable_node = OpenDeclaration(node, Declaration.Types.DeclarationType.Instance);
            var HasImplicitType = (node.Type.ToString() == "var");

            variable_node.DeclData.Name = GetLocatedName(node.Identifier);
            variable_node.DeclData.InstanceData = new InstanceDeclData() {
                ImplicitType = HasImplicitType,
                Type = GetInstanceTypeData(node),
            };
            CloseDeclaration();

            base.VisitForEachStatement(node);

            CloseContext();
        }

//         public override void VisitMemberAccessExpression(MemberAccessExpressionSyntax node) {
//             var (src, uid, qualName) = GetIdentifierName(node.Expression);
//             AddUse(src, uid, qualName, GetNodeRange(node.Expression));
//             base.VisitMemberAccessExpression(node);
//         }

        /// Visit a symbol which corresponds to an identifier and record its use.
        public override void VisitIdentifierName(IdentifierNameSyntax node) {
            UseNode(node);
            base.VisitIdentifierName(node);
        }

        /// <summary>
        /// Constructs a new builder, which will be using the provided
        /// semantic model to resolve symbol information.
        /// </summary>
        /// <param>
        /// ModelP --- the semantic model used to get symbol information
        /// </param>
        /// <note>
        /// Use the build method to actually build a DU Chain.
        /// </note>
        public DUBuilder(SemanticModel ModelP)
        {
            nodeStack.Push(root);
            model = ModelP;
        }

        /// Builds and returns a DU Chain from a syntax tree
        public TreeNode build(SyntaxNode rootSyntaxNode) {

            // Build the DU Tree by visiting the nodes in the tree
            Visit(rootSyntaxNode);

            // Create the new DU Chain root
            // and copy the children of the root built in the previous
            // step, preceded by the collected import-nodes
            var ret = new TreeNode() {};
            foreach(var n in imports.Values)
                ret.Children.Add(n);
            foreach(var n in root.Children)
                ret.Children.Add(n);
            return ret;
        }
    }
}
