
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

using ICSharpCode.Decompiler.CSharp;
using ICSharpCode.Decompiler.TypeSystem;
using ICSharpCode.Decompiler.Metadata;
using ICSharpCode.Decompiler.Disassembler;

namespace Ducserver {
    class DecompilerDB {

        Dictionary<string, List<string>> dllToSourcesMap = new Dictionary<string, List<string>>();

        /// Path to the directory where decompiled dlls are stored
        private static string baseCachePath = Path.Combine(
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
            "duserver_csharp_decompiler_cache"
        );

        /// Computes a SHA256 checksum of a given file
        private static string GetChecksum(string path)
        {
            using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 16 * 1024 * 1024))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] checksum = sha.ComputeHash(stream);
                return BitConverter.ToString(checksum).Replace("-", String.Empty);
            }
        }

        /// Returns the directory where to store decompiled sources
        /// for the given dll
        private static string GetCachePathForDll(string dllPath) {
            var checkSum = GetChecksum(dllPath);
            return Path.Combine(baseCachePath, Path.GetFileName(dllPath), checkSum);
        }

        /// Decompiles the assembly located at @p dllPath
        /// into the cache directory (if it does not exist)
        public void Decompile(string dllPath) {
            var cachePath = GetCachePathForDll(dllPath);
            if (! Directory.Exists(cachePath)) {
                var decompiler = new WholeProjectDecompiler();
			    var module = new PEFile(dllPath);
			    var resolver = new UniversalAssemblyResolver(dllPath, false, module.Reader.DetectTargetFrameworkId());
			    decompiler.AssemblyResolver = resolver;
                Console.WriteLine("Decompiling "+dllPath+" into "+cachePath);
                Directory.CreateDirectory(cachePath);
			    decompiler.DecompileProject(module, cachePath);
                DirectoryInfo dirInfo = new DirectoryInfo(cachePath);
                dllToSourcesMap[dllPath] = dirInfo.GetFiles("*.cs", SearchOption.AllDirectories).Select(f => f.FullName).ToList();
            }
        }

        /// Gets a list of decompiled source files for the assembly @p dllPath
        public List<string> GetDecompiledSources(string dllPath) {
            if (dllToSourcesMap.ContainsKey(dllPath)) {
                return dllToSourcesMap[dllPath];
            }
            return new List<string>();
        }
    }
}
