﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;


using Google.Protobuf;
using Grpc.Core;
using Grpc.Core.Utils;
using Ducproto;

using Ducserver;


namespace Ducserver
{
    // Comment
    public class DUServerImpl : DUServer.DUServerBase
    {

        public override Task<ParseResponse> Parse(ParseRequest request, ServerCallContext context)
        {
            Console.WriteLine($"Serving request on thread: {Thread.CurrentThread.Name}");
            Console.WriteLine("Got request to parse" + request.Path);
            var program_src = request.Source.Length > 0 ? request.Source : File.ReadAllText(request.Path);
            Console.WriteLine("=========================================================");
            Console.Write(program_src);
            Console.WriteLine("=========================================================");

            var proj = ProjectDB.GetProjectForSource(request.Path);
            var (semanticModel, syntaxTreeRoot) = proj.ParseSource(program_src, request.Path);

            var duBuilder = new DUBuilder(semanticModel);
            Console.WriteLine("Start Visiting");
            var duRoot = duBuilder.build(syntaxTreeRoot);
            Console.WriteLine("Stop Visiting");
            var response = new ParseResponse();
            response.Root = duRoot;
            Console.WriteLine("Sending Parse Result");
            return Task.FromResult(response);
        }
        public async override Task<CompletionResponse> Complete(CompletionRequest request, ServerCallContext context)
        {
            Console.WriteLine("Got request for completion in " + request.Path + " at " + request.Position.ToString());
            var programSrc = request.Source.Length > 0 ? request.Source : File.ReadAllText(request.Path);
            var proj = ProjectDB.GetProjectForSource(request.Path);
            var (doc, complSvc) = proj.GetCompletionService(request.Path, programSrc);
            var compl =  await complSvc.GetCompletionsAsync(doc, request.Position);
            var complContext = programSrc.Substring(request.ContextStart, request.Position-request.ContextStart);
            Console.WriteLine("Context: >>["+ complContext +"]<<");
            var response = new CompletionResponse();
            if (compl != null)
                foreach(var item in compl.Items) {
                    var chg = await complSvc.GetChangeAsync(doc, item);
                        response.Items.Add(new CompletionItem() {
                            ReplaceText = chg.TextChange.NewText,
                            ReplaceStart = chg.TextChange.Span.Start,
                            ReplaceEnd = chg.TextChange.Span.End,
                            CurPos = chg.NewPosition ?? chg.TextChange.Span.End+1
                        });
                        Console.WriteLine("    "+chg.TextChange.NewText+"("+chg.TextChange.Span.ToString()+")");
                }
            Console.WriteLine("Sending Completion Result");
            return response;
        }
    }
    class Program
    {
        async static Task RunServer(string host, int port) {
            Server server = new Server
            {
                Services = { DUServer.BindService(new DUServerImpl()) },
                Ports = { new ServerPort(host, port, ServerCredentials.Insecure) }
            };
            server.Start();
            await Task.Factory.StartNew(() =>
            {
                Console.WriteLine("DU server listening on port: " + server.Ports.First().BoundPort);
                Console.WriteLine("Press any key to stop the server...");
                try {
                    Console.ReadKey();
                } catch {
                    while (true) {};
                }
            });
            server.ShutdownAsync().Wait();
        }

        public static (Dictionary<String, String>, List<string>) ParseCommandLine(String[] args) {
            var options = new Dictionary<String,String>();
            var commands = new List<String>();
            String current_option = "";
            foreach(var arg in args) {
                if (arg.StartsWith("--") || arg.StartsWith("-")) {
                    if (current_option.Length > 0)
                        options[current_option] = "";
                    current_option = arg.TrimStart('-');
                } else if (current_option.Length > 0) {
                    options[current_option] = arg;
                    current_option = "";
                } else {
                    commands.Add(arg);
                }
            }
            Console.Write("Got commands: ");
            foreach(var cmd in commands)
                Console.Write(cmd+";");
            Console.WriteLine("");
            return (options, commands);
        }

        public static string default_command = "server";
        public static int default_port = ServerPort.PickUnused;
        public static string default_host = "localhost";

        async static Task Main(String[] args)
        {
            var (options, commands) = ParseCommandLine(args);
            if (commands.Count() == 0 )
                commands.Add(default_command);
            switch(commands.First()) {
                case "server":
                    var port = options.ContainsKey("port") ? Convert.ToInt32(options["port"]) : default_port;
                    var host = options.ContainsKey("host") ? options["host"] : default_host;
                    await RunServer(host, port);
                    break;
                case "info":
                    break;
                case "duchain":
                    var service = new DUServerImpl();
                    var request = new ParseRequest() {
                        Path = commands[1]
                    };
                    var response = await service.Parse(request, null);
                    var format = options.ContainsKey("format") ? options["format"].ToLower() : "json";
                    var formatter = new ChainFormatter(response.Root);
                    String outStr = "";
                    switch( format ) {
                        case "json":
                            outStr = formatter.ToJSON();
                            break;
                        case "txt":
                            outStr = formatter.ToTXT();
                            break;
                        default:
                            break;
                    };
                    if (options.ContainsKey("output")) {
                        File.WriteAllText(options["output"], outStr);
                    } else {
                        Console.Write(outStr);
                    }
                    break;
            }

        }
    }
}
