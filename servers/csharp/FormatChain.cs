using System;

using Google.Protobuf;
using Grpc.Core;
using Grpc.Core.Utils;

using Ducproto;

namespace Ducserver {
    class ChainFormatter {

        private TreeNode root;

        private int indent = 0;
        private String indentString = "  ";

        protected String _ToTXT(TreeNode node) {
            if (node == null)
                return "";
            var ret = "";
            switch( node.Type ) {
                case TreeNode.Types.NodeType.Declaration:
                    var loc = (node.DeclData.Name?.Location != null) ?
                                    node.DeclData.Name.Location :
                                    node.SourceLocation;
                    var loc_string = loc.Start.Line.ToString()+":"+
                                     loc.Start.Column.ToString()+"-"+
                                     loc.End.Column.ToString()+"-";
                    String prefix = "";
                    for(var i = 0; i < indent; i++) prefix += indentString;
                    for(var i = 0; i < 9-loc_string.Length; i++) prefix += " ";
                    ret = loc_string + prefix + "Decl("+node.DeclData.Kind+") "+node.DeclData.DeclUid + "\n";
                    break;
                default:
                    break;
            }
            indent += 1;
            foreach(var ch in node.Children)
                ret += _ToTXT(ch);
            indent -= 1;
            return ret;
        }

        public ChainFormatter(TreeNode pRoot) {
            root = pRoot;
        }

        public String ToJSON() {
            var formatter = JsonFormatter.Default;
            return formatter.Format(root);
        }

        public String ToTXT() {
            return _ToTXT(root);
        }
    }
}
