// Copyright (c) Microsoft. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for full license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Microsoft.Build.Construction;
using Microsoft.Build.Evaluation;
using Microsoft.Build.Framework;
using Microsoft.Build.Execution;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Completion;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Host.Mef;
using Microsoft.CodeAnalysis.Text;

using Buildalyzer;

using Ducserver;

namespace Ducserver
{

    class ProjectDB {
        private static Dictionary<String, CSharpProject> projectMap = new Dictionary<String, CSharpProject>();
        private static Dictionary<String, String> files2Project = new Dictionary<String, String>();

        public static DecompilerDB decompiler = new DecompilerDB();
        public static string FindProjectFileForSource(string sourcePath) {
            var dir = Directory.GetParent(sourcePath);
            while (dir.Root.FullName != dir.FullName) {
                var projFile = Directory.GetFiles(dir.FullName, "*.csproj", SearchOption.TopDirectoryOnly);
                if (projFile.Length > 0) return projFile[0];
                dir = Directory.GetParent(dir.FullName);
            }
            return null;
        }

        public static CSharpProject GetProjectForSource(string sourcePath) {
            if (! files2Project.ContainsKey(sourcePath))
                files2Project[sourcePath] = FindProjectFileForSource(sourcePath);
            var projFile = files2Project[sourcePath];
            if (! projectMap.ContainsKey(projFile??""))
                projectMap[projFile??""] = new CSharpProject(projFile);
            return projectMap[projFile??""];
        }

    }
    class CSharpProject {
        private static String[] resolveTargets = {
            "ResolveProjectReferences",
            "ResolveAssemblyReferences"
        };

        private Microsoft.Build.Evaluation.Project project;
        private Microsoft.CodeAnalysis.Project msCAProject;

        private IEnumerable<PortableExecutableReference> cached_references;
        private IEnumerable<string> cached_all_sources = new string[] {};
        private AnalyzerResult cachedAnalyzerResult;

        private Dictionary<String,SyntaxTree> parsedFileTrees = new Dictionary<string, SyntaxTree>();

        private CSharpCompilation compilation;

        private AdhocWorkspace workspace;

        private ProjectInfo projInfo;
        private string projName;

        public CSharpProject(String projectFile) {
            try {
                MSBuildHelper.RegisterProject(projectFile);
                var loadedMatchingProjects = Microsoft.Build.Evaluation.ProjectCollection.GlobalProjectCollection.LoadedProjects.Where(
                    proj => proj.FullPath == projectFile
                );
                if (loadedMatchingProjects.Count() == 0)
                    project = new Microsoft.Build.Evaluation.Project(projectFile);
                else
                    project = loadedMatchingProjects.First();
            } catch {
                project = new Microsoft.Build.Evaluation.Project();
                cached_references = new[] { MetadataReference.CreateFromFile(typeof(object).Assembly.Location) };
            }

            var host = MefHostServices.Create(MefHostServices.DefaultAssemblies);
            workspace = new AdhocWorkspace(host);
            projName = projectFile?.Replace(".csproj", "")??"__adhoc__";
            projInfo = ProjectInfo.Create(ProjectId.CreateNewId(), VersionStamp.Create(), projName, projName, LanguageNames.CSharp);

            Console.WriteLine("Project File: "+projectFile??"None");
            compilation = CSharpCompilation.Create(projectFile?.Replace(".csproj", ".dll")??"__adhoc__.dll");
            AddReferences();

            /* if ( cachedAnalyzerResult != null )
                foreach(var src in cached_all_sources)
                    workspace.AddDocument(msCAProject.Id, src, SourceText.From(File.ReadAllText(src))); */
        }

        public (Document, CompletionService) GetCompletionService(string source_path, string source) {
            var document = workspace.AddDocument(msCAProject.Id, source_path??"__adhoc__.cs", SourceText.From(source));
            return (document, CompletionService.GetService(document));
        }

        public void AddReferences(bool refresh = false) {
            if (refresh || cachedAnalyzerResult is null ) {
                if (project.FullPath is null) {
                    var mscorlib = MetadataReference.CreateFromFile(typeof(object).Assembly.Location);
                    cached_references = new[] { mscorlib };
                    msCAProject = workspace.AddProject(projInfo).AddMetadataReferences(cached_references);
                } else {
                    AnalyzerManager manager = new AnalyzerManager();
                    ProjectAnalyzer analyzer = manager.GetProject(project.FullPath);
                    AnalyzerResults results = analyzer.Build();
                    cachedAnalyzerResult = results.Single();
                    var mscorlib = MetadataReference.CreateFromFile(typeof(object).Assembly.Location);
                    var references = new[] { mscorlib };
                    cached_references = references.Concat(cachedAnalyzerResult.References.Select(path => MetadataReference.CreateFromFile(path)));
                    msCAProject = workspace.AddProject(projInfo).AddMetadataReferences(cached_references);
                    foreach(var assemblyPath in cachedAnalyzerResult.References) {
                        Console.WriteLine("Scheduling decompiling of "+assemblyPath);
                        Task.Run(() => {
                            ProjectDB.decompiler.Decompile(assemblyPath);
                            foreach(var src in ProjectDB.decompiler.GetDecompiledSources(assemblyPath)) {
                                Console.WriteLine("Adding decompiled source "+src);
                                AddSource(src);
                            }
                        });
                    }
                }
            }
            compilation = compilation.AddReferences(cached_references);
            if ( cachedAnalyzerResult != null )
                foreach(var src in cachedAnalyzerResult.SourceFiles) {
                    AddSource(src);
                }
        }

        public IEnumerable<PortableExecutableReference> GetReferences(bool refresh = false) {
            if (refresh || cached_references is null ) {
                AnalyzerManager manager = new AnalyzerManager();
                ProjectAnalyzer analyzer = manager.GetProject(project.FullPath);
                AnalyzerResults results = analyzer.Build();
                AnalyzerResult result = results.Single();

                foreach(var r in result.References) Console.WriteLine(r);
                var mscorlib = MetadataReference.CreateFromFile(typeof(object).Assembly.Location);
                var references = new[] { mscorlib };
                cached_references = references.Concat(result.References.Select(path => MetadataReference.CreateFromFile(path)));
            }
            return cached_references;
        }

        private BuildResult BuildTargets(string[] Targets) {
            return Microsoft.Build.Execution.BuildManager.DefaultBuildManager.Build(
                new BuildParameters() {
                    Loggers = new[] {new Logger()}
                },
                new BuildRequestData(project.CreateProjectInstance(), Targets)
            );
        }

        public void AddSource(string path) {
            cached_all_sources = cached_all_sources.Append(path);
            SyntaxTree syntaxTree = null;
            parsedFileTrees.TryGetValue(path, out syntaxTree);
            if (syntaxTree is null) {
                Console.WriteLine("Parsing "+path+" (adding to project)");
                syntaxTree = CSharpSyntaxTree.ParseText(File.ReadAllText(path), path: path);
                parsedFileTrees[path] = syntaxTree;
            }
            if (!compilation.ContainsSyntaxTree(syntaxTree))
                compilation = compilation.AddSyntaxTrees(new[] {syntaxTree});
            workspace.AddDocument(msCAProject.Id, path, SourceText.From(File.ReadAllText(path)));
        }

        public (SemanticModel, SyntaxNode) ParseSource(string code, string path=null) {
            SyntaxTree oldTree = null, newTree = CSharpSyntaxTree.ParseText(code, path: path);
            parsedFileTrees.TryGetValue(path??"", out oldTree);
            if (! (path is null)) parsedFileTrees[path] = newTree;
            if (oldTree is null || !compilation.ContainsSyntaxTree(oldTree)) compilation = compilation.AddSyntaxTrees(new[] {newTree});
            else compilation = compilation.ReplaceSyntaxTree(oldTree, newTree);
            return (compilation.GetSemanticModel(newTree), newTree.GetRoot());
        }

        public CSharpCompilation GetCompilation() {
            return compilation;
        }

    }
    class Logger : ILogger
    {
        public void Initialize(IEventSource eventSource) {
            eventSource.AnyEventRaised += (_, args) => { Console.WriteLine(args.Message); };
        }
        public void Shutdown() {}
        public LoggerVerbosity Verbosity { get; set; }
        public string Parameters { get; set; }
    }
}
