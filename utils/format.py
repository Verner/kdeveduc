#!/usr/bin/env python3
import sys

import grpc

import duc_pb2_grpc as client
import duc_pb2 as proto

from collections import defaultdict
from pathlib import Path

from term import FG, BG, get_terminal_size



def getDeclKind(kind):
    return proto._DECLARATION_DECLARATIONTYPE.values_by_number[kind].name

class Coloring:
    def __init__(self):
        self._colors = {}
        self._pos = 1
        self._r = 7
        self._g = 9
        self._b = 10

    def get_color(self, decl_uid):
        if decl_uid not in self._colors:
            self._colors[decl_uid] = FG((
                (self._pos % self._r)*10+100,
                (self._pos % self._g)*10+100,
                (self._pos % self._b)*10+100
            ))
            self._pos += 1
        return self._colors[decl_uid]

    def get_colored(self, decl_uid):
        return self.get_color(decl_uid) | decl_uid




class FormattedSource:
    def __init__(self, path):
        self.lines = open(path).readlines()
        self.hilights = defaultdict(list)
        self.annotations = defaultdict(list)
        self.colors = Coloring()
        self.width = get_terminal_size(default=(0, 0))[0]
        self.marked_lines = {}

    def visit(self, du_node):
        if du_node.type == proto.TreeNode.DECLARATION:
            data = du_node.decl_data
            if data.name:
                loc = data.name.location
            else:
                loc = du_node.source_location
            self.hilights[loc.start.line].append((loc.start.column, loc.end.column, self.colors.get_color(data.decl_uid)))
            self.annotations[loc.start.line].append(du_node)

        elif du_node.type == proto.TreeNode.USE:
            loc = du_node.source_location
            data = du_node.use_data
            self.hilights[loc.start.line].append((loc.start.column, loc.end.column, self.colors.get_color(data.decl_uid)))
            self.annotations[loc.start.line].append(du_node)

        for ch in du_node.children:
            self.visit(ch)

    def mark_line(self, ln, mark=BG((60,60,60))):
        self.marked_lines[ln] = mark

    def _get_hi_line(self, ln):
        src_line = self.lines[ln]
        line = ""
        pos = 0
        for (scol, ecol, color) in self.hilights[ln]:
            if scol < pos:
                scol = pos
            line += src_line[pos:scol] + (color | src_line[scol:ecol+1])
            pos = max(ecol+1, pos)
        line += src_line[pos:]
        return line.rstrip()+(self.width-4-len(src_line))*" ";

    def _print_line(self, ln, filter_types=None):
        hi_source = self._get_hi_line(ln)
        mark = self.marked_lines.get(ln, BG((30, 30, 30)))
        print(mark | "{ln:3}  {src}".format(ln=ln, src=hi_source))
        if filter_types is None:
            annots = self.annotations[ln]
        else:
            annots = [n for n in self.annotations[ln] if n.type in filter_types]
        for node in annots:
            if node.type == proto.TreeNode.DECLARATION:
                print("     {indent}DECL:{kind} {uid}".format(indent=node.source_location.start.column*" ",
                                                             kind=getDeclKind(node.decl_data.kind),
                                                             uid=self.colors.get_colored(node.decl_data.decl_uid)
                                                    )
                )
            elif node.type == proto.TreeNode.USE:
                print("     {indent}USE {uid} ({qual})".format(indent=node.source_location.start.column*" ",
                                                             uid=self.colors.get_colored(node.use_data.decl_uid),
                                                             qual=".".join(node.use_data.qualified_name)
                                                    )
                )
            elif node.type == ComplItem.TYPE:
                print("     {indent}{compl}".format(indent=node.start_col*" ", compl=node.replace_text))


    def print(self, filter_types=None, start_line=None, stop_line=None):
        start_line = start_line or 0
        stop_line = min(stop_line or len(self.lines), len(self.lines))
        for ln in range(start_line, stop_line):
            self._print_line(ln, filter_types)

def parse(path, port=3569):
    with grpc.insecure_channel('localhost:{port}'.format(port=port)) as channel:
        req = proto.ParseRequest(path=path)
        stub = client.DUServerStub(channel)
        response = stub.Parse(req)
        return response.root

def complete(src, path, pos, port=3569):
    with grpc.insecure_channel('localhost:{port}'.format(port=port)) as channel:
        req = proto.CompletionRequest(source=src, path=path, position=pos)
        stub = client.DUServerStub(channel)
        response = stub.Complete(req)
        return [ComplItem(src, it) for it in response.items]


def lc2pos(src: str, ln: int, col: int):
    lines = src.split('\n')
    return len('\n'.join(lines[:ln]))+col


def pos2lc(src: str, pos: int):
    prev_lines = src[:pos].split('\n')
    ln = len(prev_lines)-1
    col = pos-len("\n".join(prev_lines[:-1]))
    return ln, col-1


class ComplItem:
    TYPE = "CPL"

    def __init__(self, src, item):
        self.start_ln, self.start_col = pos2lc(src, item.replace_start)
        self.stop_ln, self.stop_col = pos2lc(src, item.replace_end)
        self.replace_text = item.replace_text
        self.cur_ln, self.cur_col = pos2lc(src, item.cur_pos)
        self.type = self.TYPE

if __name__ == "__main__":
    path = str(Path(sys.argv[1]).absolute())
    if len(sys.argv) > 2:
        port = int(sys.argv[2])
    else:
        port = 3569
    du_chain = parse(path, port)
    src = FormattedSource(path)
    src.visit(du_chain)
    if len(sys.argv) == 4:
        src_text = Path(path).read_text()
        ln, col = map(int, sys.argv[3].split(':', 2))
        pos = lc2pos(src_text, ln, col)
        src.annotations[ln].append(ComplItem(src_text, proto.CompletionItem(replace_start=pos, replace_end=pos, replace_text="^", cur_pos=pos)))
        src.annotations[ln].extend(complete(src_text, path, pos))
        src.mark_line(ln)
        src.print(filter_types=[ComplItem.TYPE], start_line=ln-3, stop_line=ln+3)
    else:
        src.print()


