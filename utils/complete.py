#!/usr/bin/env python3

import sys

import grpc

import duserver_pb2_grpc as client
import duserver_pb2 as proto

from pathlib import Path

from term import FG, BG, get_terminal_size

def complete(src, path, pos, port=3569):
    with grpc.insecure_channel('localhost:{port}'.format(port=port)) as channel:
        req = proto.CompletionRequest(source=src, path=path, position=pos)
        stub = client.DUServerStub(channel)
        response = stub.Complete(req)
        return response.items


def lc2pos(src: str, ln: int, col: int):
    lines = src.split('\n')
    return len('\n'.join(lines[:(ln-1)]))+col

def pos2lc(src: str, pos: int):
    prev_lines = src[:pos].split('\n')
    ln = len(prev_lines)
    col = pos-len("\n".join(prev_lines))
    return ln, col

if __name__ == "__main__":
    path = Path(sys.argv[1])
    ln, col = sys.argv[2].split(':', 2)
    src = path.read_text()
    pos = lc2pos(src, int(ln), int(col))
    completions = complete(src, str(path), pos)
    for item in completions.items:
        print(item.replace_text)
