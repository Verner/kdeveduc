#!/bin/bash
# GRPC_VERBOSITY=DEBUG GRPC_TRACE=api,client_channel,pick_first,round_robin,glb
if [ "z$1" == "zgdb" ]; then
  PROG="gdb kdevelop";
else
  PROG="kdevelop";
fi;

PATH=$PATH:~/zdroj/local/bin QT_LOGGING_RULES="*.debug=false;*.warning=false;kdevelop.plugins.educ.debug=true;" QT_PLUGIN_PATH=$QT_PLUGIN_PATH:~/zdroj/local/lib/x86_64-linux-gnu/plugins $PROG -s test_duserver

