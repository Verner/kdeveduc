#pragma once

#include <unordered_map>

#include <QList>

#include <language/duchain/topducontext.h>
#include <language/duchain/identifier.h>
#include <language/duchain/types/abstracttype.h>

#include "duc.pb.h"

namespace KDevelop {
    class DUContext;
    class IndexedString;
}

namespace educ {

    /* Builds a KDevelop DU Chain for document @p url from the Abstract DU chain
     * with root @p root.
     *
     * If the document already as a DU Chain, it first deletes it and then constructs
     * a new one.
     *
     * Returns the TopDUContext corresponding to the root of the newly created DU Chain.*/
    KDevelop::ReferencedTopDUContext& buildTopDocumentContext(const KDevelop::IndexedString& url, const ducproto::TreeNode* root);

    /* A type for holding all the uses for the document (uses are processed after constructing the Contexts/Declaration tree) */
    typedef QList<std::tuple<const ducproto::Use*, KDevelop::RangeInRevision, KDevelop::DUContext*>> UseList;

    /* A dictionary type used for storing mapping declaration uids (as passed by the parser) to actual KDevelop declarations. */
    typedef std::unordered_map<std::string, KDevelop::Declaration*> DeclMap;


    /* The DUBuilder class for transforming an Abstract DU chain into a KDevelop DU Chain */
    class DUBuilder {

    public:
        /* Initializes the private datastructures for a DUBuilder instance.
         *
         * @p url is the url of the document whose DU Chain is built
         * @p root is the Abstract DU Chain which will be converted to the KDevelop DU CHain */
        DUBuilder(const KDevelop::IndexedString& url, const ducproto::TreeNode* root): m_url(url), m_root(root), m_top_context(nullptr) {};

        /* Builds the KDevelop DU Chain and returns its root */
        KDevelop::ReferencedTopDUContext& build();

    protected:
        /* A recursive node visitor which builds the DU Chain corresponding to the Abstract DU Chain subtree
         * rooted at @p node. The resulting KDevelop chain will be rooted at @p parent */
        void build(const ducproto::TreeNode &node, KDevelop::DUContext* parent);

        /* Runs as the last step of the build process and constructs the uses from the m_use_list */
        void build_uses();

        /* Processes an import node, scheduling a parse job for the imported file, if necessary */
        void process_import(const ducproto::Import& data);

        /* Functions which deal with the different Abstract DU Chain nodes. Each returns the constructed du-context corresponding
         * to the node */
        KDevelop::DUContext* process_namespace_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, KDevelop::DUContext* parent);
        KDevelop::DUContext* process_alias_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, KDevelop::DUContext* parent);
        KDevelop::DUContext* process_function_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, KDevelop::DUContext* parent);
        KDevelop::DUContext* process_class_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, KDevelop::DUContext* parent);
        KDevelop::DUContext* process_method_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, KDevelop::DUContext* parent);
        KDevelop::DUContext* process_instance_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, KDevelop::DUContext* parent);

        /* A helper function which creates a new DU Context instance and adds it as a child to @parent. Its
         * source code location is initialized from @p range. Based on its type it is either included or excluded from the
         * SymbolTable and if the identifier is provided & non-empty, its last component will identify the local scope
         * corresponding to the Context instance */
        KDevelop::DUContext* create_context(KDevelop::DUContext* parent, KDevelop::RangeInRevision range, const KDevelop::QualifiedIdentifier& identifier = KDevelop::QualifiedIdentifier(), KDevelop::DUContext::ContextType type = KDevelop::DUContext::Other);

        /* Creates & returns an Abstract type based on @p data. The @p ctx is used for looking up the type definition.*/
        KDevelop::AbstractType::Ptr getType(const ducproto::TypeData data, KDevelop::DUContext* ctx);

        /* Creates a top-level stub declaration for a class with no known declaration. */
        KDevelop::Declaration* declare_unknown_class(KDevelop::DUContext* parent, QString qualName);

        /* Updates the m_decl_map storing the @p decl pointer under the @p data.uid() key. */
        void register_declaration(KDevelop::Declaration* decl, const ducproto::Declaration& data);

    private:
        /* The url of the document the builder is constructing the DU Chain for */
        const KDevelop::IndexedString& m_url;

        /* The root of the Abstract DU Chain */
        const ducproto::TreeNode* m_root;

        /* The root of the KDevelop DU Chain */
        KDevelop::ReferencedTopDUContext m_top_context;

        /* A list of encountered Abstract Use nodes, their source locations and the corresponding contexts, where they occur. */
        UseList m_use_list;

        /* A dictionary mapping declaration UIDs to Declaration pointers (used by build_uses to lookup the use definitions) */
        DeclMap m_decl_map;
    };

}

