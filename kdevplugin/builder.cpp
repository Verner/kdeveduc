#include <kdevplatform/interfaces/icore.h>
#include <kdevplatform/interfaces/ilanguagecontroller.h>
#include <kdevplatform/language/backgroundparser/backgroundparser.h>

#include <kdevplatform/language/duchain/duchain.h>
#include <kdevplatform/language/duchain/duchainlock.h>
#include <kdevplatform/language/duchain/duchainpointer.h>
#include <kdevplatform/language/duchain/duchainutils.h>
#include <kdevplatform/language/duchain/ducontext.h>
#include <kdevplatform/language/duchain/topducontext.h>
#include <kdevplatform/language/duchain/identifier.h>

#include <kdevplatform/language/duchain/declaration.h>
#include <kdevplatform/language/duchain/classdeclaration.h>
#include <kdevplatform/language/duchain/classfunctiondeclaration.h>
#include <kdevplatform/language/duchain/functiondeclaration.h>

#include <kdevplatform/language/duchain/types/arraytype.h>
#include <kdevplatform/language/duchain/types/containertypes.h>
#include <kdevplatform/language/duchain/types/functiontype.h>
#include <kdevplatform/language/duchain/types/integraltype.h>
#include <kdevplatform/language/duchain/types/pointertype.h>
#include <kdevplatform/language/duchain/types/referencetype.h>
#include <kdevplatform/language/duchain/types/structuretype.h>



#include <debug.h>
#include "kdevplugin.h"
#include "helpers.h"

#include "builder.h"



using namespace KDevelop;
using namespace educ;
using namespace ducproto;

/***********************************************************************
 *                         HELPER FUNCTIONS                            *
 ***********************************************************************/


QPair<int, int> inline node_start_pos(const TreeNode& node)
{
    if (!node.has_source_location() || !node.source_location().has_start()) return QPair<int,int>(0,0);
    return QPair<int,int>(node.source_location().start().line(), node.source_location().start().column());
}

QPair<int, int> inline node_end_pos(const TreeNode& node)
{
    if (!node.has_source_location() || !node.source_location().has_end()) return QPair<int,int>(INT_MAX,INT_MAX);
    return QPair<int,int>(node.source_location().end().line(), node.source_location().end().column());
}

RangeInRevision inline node_range(const TreeNode& node) {
    auto [s_line, s_col] = node_start_pos(node);
    auto [e_line, e_col] = node_end_pos(node);
    return RangeInRevision(s_line, s_col, e_line, e_col );
}

RangeInRevision inline name_range(const LocatedName& name) {
    auto start = name.location().start();
    auto end = name.location().end();
    return RangeInRevision(start.line(), start.column(), end.line(), end.column());
}

std::string join(const google::protobuf::RepeatedPtrField<std::string> range, const std::string separator) {
    if (range.empty()) return std::string();

    return std::accumulate(
        next(begin(range)), // there is at least 1 element, so OK.
        end(range),

        *range.begin(), // the initial value

        [&separator](std::string result, const std::string &value) {
            return result + separator + value;
    });
};

QualifiedIdentifier strToIdent(const std::string& ident, DUContext* parent) {
    QualifiedIdentifier ret = parent->scopeIdentifier();
    ret.push(Identifier(QString::fromStdString(ident)));
    return ret;
}

IndexedIdentifier strToIndexedIdent(const std::string& ident, DUContext* parent) {
    return IndexedIdentifier(strToIdent(ident, parent).last());
}

inline IntegralType::CommonIntegralTypes builtinToIntegral(const BuiltinTypeData::BuiltInType tp) {
    switch(tp) {
        case ducproto::BuiltinTypeData_BuiltInType_VOID: return IntegralType::TypeVoid;
        case ducproto::BuiltinTypeData_BuiltInType_NONE: return IntegralType::TypeNone;
        case ducproto::BuiltinTypeData_BuiltInType_NULL_: return IntegralType::TypeNull;
        case ducproto::BuiltinTypeData_BuiltInType_CHAR: return IntegralType::TypeChar;
        case ducproto::BuiltinTypeData_BuiltInType_BOOLEAN: return IntegralType::TypeBoolean;
        case ducproto::BuiltinTypeData_BuiltInType_BYTE: return IntegralType::TypeByte;
        case ducproto::BuiltinTypeData_BuiltInType_SBYTE: return IntegralType::TypeSbyte;
        case ducproto::BuiltinTypeData_BuiltInType_SHORT: return IntegralType::TypeShort;
        case ducproto::BuiltinTypeData_BuiltInType_INT: return IntegralType::TypeInt;
        case ducproto::BuiltinTypeData_BuiltInType_LONG: return IntegralType::TypeLong;
        case ducproto::BuiltinTypeData_BuiltInType_FLOAT: return IntegralType::TypeFloat;
        case ducproto::BuiltinTypeData_BuiltInType_DOUBLE: return IntegralType::TypeDouble;
        case ducproto::BuiltinTypeData_BuiltInType_WCHAR: return IntegralType::TypeWchar_t;
        case ducproto::BuiltinTypeData_BuiltInType_STRING: return IntegralType::TypeString;
        case ducproto::BuiltinTypeData_BuiltInType_MIXED: return IntegralType::TypeMixed;
        case ducproto::BuiltinTypeData_BuiltInType_CHAR16: return IntegralType::TypeChar16_t;
        case ducproto::BuiltinTypeData_BuiltInType_CHAR32: return IntegralType::TypeChar32_t;
    }
    return IntegralType::TypeNone;
};

AbstractType::Ptr findType(const std::string& ident, DUContext* ctx) {
    auto decl = getTypeDeclaration(QualifiedIdentifier(QString::fromStdString(ident)), ctx);
    if (decl) return decl->abstractType();
    return AbstractType::Ptr(new UnsureType);
}

KDevelop::AbstractType::Ptr educ::DUBuilder::getType(const ducproto::TypeData data, KDevelop::DUContext* ctx)
{
    auto qualName = QString::fromStdString(data.name().name());
    auto qualID = QualifiedIdentifier(qualName);
    qCDebug(PLUGIN_EDUC) << "         Searching for type " << qualName;
    auto decl = getTypeDeclaration(qualID, ctx);
    if (decl) {
        qCDebug(PLUGIN_EDUC) << "           Found" << decl->abstractType()->toString();
        return decl->abstractType();
    }
    qCDebug(PLUGIN_EDUC) << "           Not Found" << qualName;
    switch(data.kind()) {
        case ducproto::TypeData_TypeKind_BUILTIN:
            return AbstractType::Ptr(new IntegralType(builtinToIntegral(data.builtin_data().kind())));
        case ducproto::TypeData_TypeKind_FUNCTION:
            return AbstractType::Ptr(new FunctionType);
        case ducproto::TypeData_TypeKind_POINTER: {
            auto ref = new PointerType();
            ref->setBaseType(getType(data.reference_data().refereced_type(), ctx));
            return AbstractType::Ptr(ref);
        }
        case ducproto::TypeData_TypeKind_REFERENCE: {
            auto ref = new ReferenceType();
            ref->setBaseType(getType(data.reference_data().refereced_type(), ctx));
            return AbstractType::Ptr(ref);
        }
        case ducproto::TypeData_TypeKind_STRUCTURE:
            return declare_unknown_class(ctx, qualName)->abstractType();
        case ducproto::TypeData_TypeKind_DELAYED:
            break;
        case ducproto::TypeData_TypeKind_ARRAY: {
            auto arr = new ArrayType();
            arr->setElementType(getType(data.array_data().element_type(), ctx));
            arr->setDimension(data.array_data().dimension());
            return AbstractType::Ptr(arr);
        }
        case ducproto::TypeData_TypeKind_ENUMERATION:
            return AbstractType::Ptr(new IntegralType(IntegralType::TypeEnumeration));
        case ducproto::TypeData_TypeKind_ENUMERATOR:
            return AbstractType::Ptr(new IntegralType(IntegralType::TypeEnumerator));
        case ducproto::TypeData_TypeKind_ALIASTP:
            return AbstractType::Ptr(new IntegralType(IntegralType::TypeAlias));
        case ducproto::TypeData_TypeKind_LIST: {
            auto list = new ListType();
            for(const auto& tp: data.list_data().element_types()) {
                list->addContentType<UnsureType>(getType(tp, ctx));
            }
            return AbstractType::Ptr(list);
        }
        case ducproto::TypeData_TypeKind_MAP: {
            auto map = new MapType();
            for(const auto& tp: data.map_data().element_types()) {
                map->addContentType<UnsureType>(getType(tp, ctx));
            }
            for(const auto& tp: data.map_data().key_types()) {
                map->addKeyType<UnsureType>(getType(tp, ctx));
            }
            return AbstractType::Ptr(map);
        }
        case ducproto::TypeData_TypeKind_UNSURE:
            return AbstractType::Ptr(new UnsureType);
    };
    return AbstractType::Ptr(new UnsureType);
}

inline IndexedIdentifier IndexedIdentFromDecl(const ducproto::Declaration& data) {
    return IndexedIdentifier(Identifier(IndexedString(QString::fromStdString(data.name().name()))));
}

DUContext* DUBuilder::create_context(DUContext* parent, RangeInRevision range, const QualifiedIdentifier& identifier, DUContext::ContextType type) {
    DUContext* ret = new DUContext(range, parent);
    ret->setType( type );
    if ( !identifier.isEmpty() )
        ret->setLocalScopeIdentifier( QualifiedIdentifier(identifier.last()) );
    if (!ret->parentContext()->inSymbolTable())
        ret->setInSymbolTable(false);
    else
        ret->setInSymbolTable(type == DUContext::Class || type == DUContext::Namespace || type == DUContext::Global || type == DUContext::Helper || type == DUContext::Enum);
    return ret;
}
void DUBuilder::register_declaration(KDevelop::Declaration* decl, const ducproto::Declaration& data)
{
    if (data.decl_uid().length() > 0) {
        m_decl_map[data.decl_uid()] = decl;
        qCDebug(PLUGIN_EDUC) << "    Registering decl" << QString::fromStdString(data.decl_uid()) << "//" << QString::fromStdString(data.comment());
    }

}


/***********************************************************************
 *                      DECLARATION PROCESSSORS                        *
 ***********************************************************************/

DUContext* educ::DUBuilder::process_alias_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range, KDevelop::RangeInRevision range, DUContext* parent) {
    return nullptr;
}

DUContext* educ::DUBuilder::process_function_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range, KDevelop::RangeInRevision range, DUContext* parent) {
    TypePtr<KDevelop::FunctionType> type(new FunctionType);
    type->setReturnType(getType(data.function_data().ret_type(), parent));
    auto declaration = new FunctionDeclaration(ident_range, parent);
    register_declaration(declaration, data);
    declaration->setIdentifier(IndexedIdentFromDecl(data));
    declaration->setInternalContext(parent);
    declaration->setKind(KDevelop::Declaration::Kind::Instance);
    declaration->setDeclarationIsDefinition(true);
    declaration->setExplicitlyTyped(true);
    declaration->setType<FunctionType>(type);
    declaration->setComment(QString::fromStdString(data.comment()));
    qCDebug(PLUGIN_EDUC) << "           declaring function" << type->toString();
    return create_context(parent, range, declaration->qualifiedIdentifier());
}

DUContext* educ::DUBuilder::process_method_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range, KDevelop::RangeInRevision range, DUContext* parent) {
    TypePtr<KDevelop::FunctionType> type(new FunctionType);
    auto declaration = new ClassFunctionDeclaration(ident_range, parent);
    register_declaration(declaration, data);
    declaration->setIdentifier(IndexedIdentFromDecl(data));
    declaration->setKind(KDevelop::Declaration::Kind::Instance);
    declaration->setDeclarationIsDefinition(true);
    declaration->setExplicitlyTyped(true);
    declaration->setAccessPolicy((KDevelop::Declaration::AccessPolicy) data.access());
    declaration->setComment("<i></i>"+QString::fromStdString(data.comment()));
    auto ret = create_context(parent, range, declaration->qualifiedIdentifier());
    declaration->setInternalContext(ret);
    for(const auto& arg: data.method_data().function_spec().arguments()) {
        type->addArgument(getType(arg.decl_data().instance_data().type(), parent));
        /* We need to add local declarations for each function argument as KDevelop does
         * not do this automatically. */
        auto arg_range = name_range(arg.decl_data().name());
        auto arg_decl = new KDevelop::Declaration(arg_range, ret);
        arg_decl->setKind(KDevelop::Declaration::Kind::Instance);
        arg_decl->setIdentifier(IndexedIdentFromDecl(arg.decl_data()));
        if (arg.decl_data().instance_data().implicittype() ) {
            qCDebug(PLUGIN_EDUC) << "           declaring argument" << IndexedIdentFromDecl(arg.decl_data()) << "of type: auto ("<<getType(arg.decl_data().instance_data().type(), parent)->toString()<<")";
            arg_decl->setExplicitlyTyped(false);
            arg_decl->setAbstractType(getType(data.instance_data().type(), parent));
        } else {
            qCDebug(PLUGIN_EDUC) << "           declaring argument" << IndexedIdentFromDecl(arg.decl_data()) << "of type: "<<getType(arg.decl_data().instance_data().type(), parent)->toString();
            arg_decl->setExplicitlyTyped(true);
            arg_decl->setAbstractType(getType(arg.decl_data().instance_data().type(), parent));
        }
    }
    type->setReturnType(getType(data.method_data().function_spec().ret_type(), parent));
    declaration->setType<FunctionType>(type);
    qCDebug(PLUGIN_EDUC) << "           declaring method" << type->toString();
    return ret;
}

DUContext* educ::DUBuilder::process_class_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range, KDevelop::RangeInRevision range, DUContext* parent) {
    TypePtr<KDevelop::StructureType> type(new StructureType);
    auto declaration = new ClassDeclaration(ident_range, parent);
    register_declaration(declaration, data);
    declaration->setIdentifier(IndexedIdentFromDecl(data));
    declaration->setKind((KDevelop::Declaration::Kind::Type));
    declaration->setClassType((ClassDeclarationData::ClassType) data.class_data().kind());
    declaration->setComment("<i></i>"+QString::fromStdString(data.comment()));
    for(const auto& base: data.class_data().base_classes()) {
        BaseClassInstance base_class;
        IndexedType base_type(findType(base.name().name(), parent));
        base_class.access = (KDevelop::Declaration::AccessPolicy)base.access();
        base_class.virtualInheritance = base.virtual_();
        base_class.baseClass = base_type;
        declaration->addBaseClass(base_class);
        qCDebug(PLUGIN_EDUC) << "           adding base class" << (base_type.abstractType() ? base_type.abstractType()->toString(): QString("unknown type")) << "(was"<< QString::fromStdString(base.name().name()) <<")";
    }
    type->setDeclaration(declaration);
    declaration->setType<StructureType>(type);
    qCDebug(PLUGIN_EDUC) << "           declaring class" << type->toString() << "=" << declaration->abstractType()->toString() << "(was"<< IndexedIdentFromDecl(data) <<")";
    auto child_ctx=create_context(parent, range, declaration->qualifiedIdentifier(), DUContext::Class);
    child_ctx->setOwner(declaration);
    return child_ctx;
}

KDevelop::Declaration* educ::DUBuilder::declare_unknown_class(KDevelop::DUContext* parent, QString qualName)
{
    qCDebug(PLUGIN_EDUC) << "    Registering unknown class" << qualName;
    TypePtr<KDevelop::StructureType> type(new StructureType);
    auto indexedID = IndexedIdentifier(Identifier(IndexedString(qualName)));
    auto qualID = QualifiedIdentifier(qualName);
    auto declaration = new ClassDeclaration(RangeInRevision(), parent->topContext());
    m_decl_map[qualName.toStdString()] = declaration;
    declaration->setIdentifier(indexedID);
    declaration->setKind((KDevelop::Declaration::Kind::Type));
    declaration->setComment(QString("<i></i>(declaration not found)"));
    type->setDeclaration(declaration);
    declaration->setType<StructureType>(type);
    qCDebug(PLUGIN_EDUC) << "    Declaring class" << type->toString() << "=" << declaration->abstractType()->toString() << "(was"<< indexedID <<")";
    return declaration;
}

DUContext* educ::DUBuilder::process_instance_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range, KDevelop::RangeInRevision range, DUContext* parent) {
    auto declaration = new KDevelop::Declaration(ident_range, parent);
    register_declaration(declaration, data);
    declaration->setKind(KDevelop::Declaration::Kind::Instance);
    declaration->setIdentifier(IndexedIdentFromDecl(data));
    declaration->setComment(QString::fromStdString(data.comment()));
    if (data.instance_data().implicittype() ) {
        qCDebug(PLUGIN_EDUC) << "    Declaring variable" << IndexedIdentFromDecl(data) << "of type: auto ("<<getType(data.instance_data().type(), parent)->toString()<<")";
        declaration->setExplicitlyTyped(false);
        declaration->setAbstractType(getType(data.instance_data().type(), parent));
    } else {
        qCDebug(PLUGIN_EDUC) << "    Declaring variable" << IndexedIdentFromDecl(data) << "of type: "<<getType(data.instance_data().type(), parent)->toString();
        declaration->setExplicitlyTyped(true);
        declaration->setAbstractType(getType(data.instance_data().type(), parent));
    }
    return create_context(parent, range, declaration->qualifiedIdentifier());
}

DUContext* educ::DUBuilder::process_namespace_decl(const ducproto::Declaration& data, KDevelop::RangeInRevision ident_range,  KDevelop::RangeInRevision range, DUContext* parent) {
    auto declaration = new KDevelop::Declaration(ident_range, parent);
    register_declaration(declaration, data);
    declaration->setKind(KDevelop::Declaration::Kind::Namespace);
    declaration->setIdentifier(IndexedIdentFromDecl(data));
    declaration->setComment(QString::fromStdString(data.comment()));
    return create_context(parent, range, declaration->qualifiedIdentifier(), DUContext::Namespace);
}

/***********************************************************************
 *                             BUILDERS                                *
 ***********************************************************************/

KDevelop::ReferencedTopDUContext& educ::buildTopDocumentContext(const IndexedString& url, const ducproto::TreeNode *root)
{
    DUBuilder builder(url, root);
    return builder.build();
}

KDevelop::ReferencedTopDUContext& educ::DUBuilder::build()
{
    DUChainWriteLocker lock( DUChain::lock() );

    m_top_context = DUChainUtils::standardContextForUrl(m_url.toUrl());


    /* If there was a DU chain we reuse it, otherwise we need to create one */
    if (! m_top_context) {
        ParsingEnvironmentFile* file = new ParsingEnvironmentFile(m_url);
        // file->setLanguage(DUServerLang::languageString());
        m_top_context = new TopDUContext(m_url, RangeInRevision( CursorInRevision( 0, 0 ), CursorInRevision( INT_MAX, INT_MAX ) ), file);
        m_top_context->setType( DUContext::Global );

        /* Register the DU chain */
        DUChain::self()->addDocumentChain( m_top_context );
    } else {
        // Remove all old children of the top context
        m_top_context->clearProblems();
        m_top_context->clearImportedParentContexts();
        m_top_context->deleteChildContextsRecursively();
        m_top_context->deleteLocalDeclarations();
        m_top_context->deleteUsesRecursively();
    }


    /* Build the DU chain */
    for(const TreeNode &node: m_root->children())
        build(node, m_top_context);

    build_uses();

    return m_top_context;
}

void educ::DUBuilder::build(const ducproto::TreeNode& node, KDevelop::DUContext* parent)
{
    qCDebug(PLUGIN_EDUC) << "Processing node" << QString::fromStdString(node.NodeType_Name(node.type())) << "at" << node.source_location().start().line()<<":"<<node.source_location().start().column() << "--"
                                                      << node.source_location().end().line()<<":"<<node.source_location().end().column();
    auto range = node_range(node);

    if (node.type() == node.CONTEXT) {
        DUContext* ctx = new DUContext(range, parent);
        ctx->setType((DUContext::ContextType) node.ctx_data().kind());
        for(const TreeNode &child: node.children())
            build(child, ctx);
    } else if (node.type() == node.DECLARATION) {
        DUContext* child_ctx = nullptr;
        auto ident_range = name_range(node.decl_data().name());
        switch( node.decl_data().kind()) {
            case Declaration_DeclarationType_ALIAS:
                child_ctx = process_alias_decl(node.decl_data(), ident_range, range, parent);
                break;
            case Declaration_DeclarationType_FUNCTION:
                child_ctx = process_function_decl(node.decl_data(), ident_range, range, parent);
                break;
            case Declaration_DeclarationType_METHOD:
                child_ctx = process_method_decl(node.decl_data(), ident_range, range, parent);
                break;
            case Declaration_DeclarationType_CLASS:
                child_ctx = process_class_decl(node.decl_data(), ident_range, range, parent);
                break;
            case Declaration_DeclarationType_CLASS_MEMBER:
                break;
            case Declaration_DeclarationType_FORWARD_DECLARATION:
                break;
            case Declaration_DeclarationType_NAMESPACE:
                child_ctx = process_namespace_decl(node.decl_data(), ident_range, range, parent);
                break;
            case Declaration_DeclarationType_NAMESPACE_ALIAS:
                break;
            case Declaration_DeclarationType_INSTANCE:
                child_ctx = process_instance_decl(node.decl_data(), ident_range, range, parent);
            case Declaration_DeclarationType_MACRO:
                break;
            case Declaration_DeclarationType_IMPORT:
                break;
            default:
                break;
        }
        if (child_ctx) {
            for(const TreeNode &child: node.children())
                build(child, child_ctx);
        }
    } else if (node.type() == node.USE) {
        m_use_list << std::tuple<const ducproto::Use*, KDevelop::RangeInRevision, DUContext*>(&node.use_data(), node_range(node), parent);
    } else if (node.type() == node.IMPORT) {
        process_import(node.import_data());
    }
}

void educ::DUBuilder::process_import(const ducproto::Import& data) {
    QString path = QString::fromStdString(data.path());
    QFile file(path);
    if(!file.exists())
        return;
    qCDebug(PLUGIN_EDUC) << "Importing file" << path;
    IndexedString url(path);
    TopDUContext* context = DUChain::self()->chainForDocument(url);
    m_top_context->addImportedParentContext(context);

    if (context) {
        ParsingEnvironmentFilePointer envFile = context->parsingEnvironmentFile();
        if (envFile && ! envFile->needsUpdate())
            return;
    }

    int priority = -1;
    BackgroundParser* bgparser = KDevelop::ICore::self()->languageController()->backgroundParser();
    TopDUContext::Features features = (TopDUContext::Features)(TopDUContext::ForceUpdate | TopDUContext::AllDeclarationsAndContexts);

    if (bgparser->isQueued(url))
    {
        if (bgparser->priorityForDocument(url) > priority )
            // Remove the document and re-queue it with a greater priority
            bgparser->removeDocument(url);
        else
            return;
    }
    bgparser->addDocument(url, features, priority, 0, ParseJob::FullSequentialProcessing);
}


void educ::DUBuilder::build_uses()
{
    for(auto [use, range, context]: qAsConst(m_use_list)) {
        KDevelop::Declaration* decl;

        /* Get the declaration which is used. First, try
         * to find it from an id, otherwise resort to
         * trying to find it from the qualified name */
        qCDebug(PLUGIN_EDUC) << "Looking for" << QString::fromStdString(use->decl_uid());
        auto it = m_decl_map.find(use->decl_uid());
        if (it != m_decl_map.end()) {
            decl = it->second;
            qCDebug(PLUGIN_EDUC) << "=> [from id] Using" << QString::fromStdString(use->decl_uid()) << "at" << range;
        } else {
            /* Build the Qualified Identifier of the declaration that is used */
            QualifiedIdentifier id;
            for(auto part: qAsConst(use->qualified_name()))
                id.push(Identifier(QString::fromStdString(part)));

            /* Find the appropriate declarations of the qualified ident
                * (see language/duchain/builders/abstractusebuilder.h:newUse(NodeT*)) */
            auto decls = context->findDeclarations(id, range.start);

            /* Look for unqualified name as a last resort */
            decls += context->findDeclarations(id.last(), range.start);

            /* Choose a candidate */
            QList<KDevelop::Declaration*> candidates;
            foreach (KDevelop::Declaration* declaration, decls)
                if (!declaration->isForwardDeclaration()) {
                    candidates.clear();
                    candidates.append(declaration);
                    break;
                }
            decl = !candidates.isEmpty() ? candidates.first() : nullptr;

            if (decl)
                qCDebug(PLUGIN_EDUC) << "=> [from qualid] Using" << id << "at" << range;
            else
                qCDebug(PLUGIN_EDUC) << C_RED << "=> Could not reconstruct from qualid " << C_RESET << id << "at" << range;
        }

        /* Get an index for the declaration */
        auto decl_idx = m_top_context->indexForUsedDeclaration(decl);

        /* Create the actual use */
        context->createUse(decl_idx, range);
    }
}





