#include <memory>

#include <grpc++/grpc++.h>

#include <QByteArray>
#include <QString>
#include <QProcess>
#include <QRegExp>

#include "rpcclient.h"

#include <debug.h>


using namespace ducproto;
using namespace educ;

RPCClient::RPCClient(const QString& server_path): m_server_path(server_path), m_server_proc(nullptr), m_server_pid(-1) {
    reStart();
}

RPCClient::RPCClient(const int port): m_server_pid(-1), m_server_port(port)
{
    auto server_address = "127.0.0.1:"+QString::number(m_server_port);
    m_client = ducproto::DUServer::NewStub(grpc::CreateChannel(
        server_address.toStdString(),
        grpc::InsecureChannelCredentials())
    );
}

RPCClient::~RPCClient()
{
    qCDebug(PLUGIN_EDUC) << "Stopping educserver" << name();
    if (m_server_proc)
        m_server_proc->disconnect();
    kill();
}

void RPCClient::kill()
{
    if (m_server_pid > 0 && m_server_proc && m_server_proc->state() != QProcess::ProcessState::NotRunning)
        m_server_proc->kill();
    delete m_server_proc;
}




void RPCClient::reStart()
{
    qCDebug(PLUGIN_EDUC) << "Starting educserver" << name();
    kill();
    m_server_proc = new QProcess(this);
    m_server_proc->setProcessChannelMode(QProcess::MergedChannels);
    m_server_proc->setReadChannel(QProcess::StandardOutput);
    connect(m_server_proc, QOverload<int>::of(&QProcess::finished), this, &RPCClient::reStart);
    connect(m_server_proc, &QProcess::readyReadStandardOutput, this, &RPCClient::onStdOut);
    m_server_proc->start(m_server_path, QStringList() << "server");
    m_server_pid = m_server_proc->pid();
    qCDebug(PLUGIN_EDUC) << "Started educ server" << name();
}

void educ::RPCClient::onStdOut()
{
    char buf[256];
    qint64 read;
    do {
        read = m_server_proc->readLine(buf, sizeof(buf));
        m_stdout += buf;
        if (read > 0 ) {
            processLine();
        }
    } while (read > 0);

}

void educ::RPCClient::processLine()
{
    qCDebug(PLUGIN_EDUC) << name() << ">>" << m_stdout;
    QRegExp pattern(".*listening on port: *([1-9][0-9]*).*");
    if (pattern.indexIn(m_stdout) > -1) {
        m_server_port = pattern.cap(1).toInt();
        auto server_address = "127.0.0.1:"+pattern.cap(1);
        m_client = DUServer::NewStub(grpc::CreateChannel(
            server_address.toStdString(),
            grpc::InsecureChannelCredentials())
        );
    }
    m_stdout = "";
}


bool educ::RPCClient::Parse(const QString& path, const QByteArray& contents, ParseResponse* resp)
{
    if (! m_client) return false;
    unsigned int client_connection_timeout = 10000;
    ParseRequest req;
    grpc::ClientContext context;

    // Set timeout for API
    std::chrono::system_clock::time_point deadline =
        std::chrono::system_clock::now() + std::chrono::milliseconds(client_connection_timeout);

    context.set_deadline(deadline);

    req.set_path(path.toStdString());
    req.set_source(contents.toStdString());
    grpc::Status status = m_client->Parse(&context, req, resp);
    if (! status.ok()) {
//         qCDebug(PLUGIN_EDUC) << "Request Error: " <<
//                                     QString::fromStdString(status.error_message()) <<
//                                     "(" << QString::fromStdString(status.error_details()) << ")";
    }
    return status.ok();
}

bool educ::RPCClient::Complete(const QString& path, const QByteArray& contents, const int cursor_pos, const int context_start, CompletionResponse* resp) {
    if (! m_client) return false;
    unsigned int client_connection_timeout = 10000;
    CompletionRequest req;
    grpc::ClientContext context;

    // Set timeout for API
    std::chrono::system_clock::time_point deadline =
        std::chrono::system_clock::now() + std::chrono::milliseconds(client_connection_timeout);

    context.set_deadline(deadline);

    req.set_path(path.toStdString());
    req.set_source(contents.toStdString());
    req.set_position(cursor_pos);
    req.set_context_start(context_start);
    grpc::Status status = m_client->Complete(&context, req, resp);
    if (! status.ok()) {
//         qCDebug(PLUGIN_EDUC) << "Request Error: " <<
//                                     QString::fromStdString(status.error_message()) <<
//                                     "(" << QString::fromStdString(status.error_details()) << ")";
    }
    return status.ok();

}

const QString educ::RPCClient::name()
{
    QString ret;
    if (m_server_path.length()>0) {
        ret += m_server_path;
        if (m_server_proc && m_server_proc->state() == QProcess::ProcessState::Starting)
            ret += "(starting)";
    }
    ret += "[:"+QString::number(m_server_port)+"]";
    return ret;
}


// #include "rpcclient.moc"

