#pragma once


#include <language/duchain/ducontext.h>
#include <QUrl>

using namespace KDevelop;

// FIXME: The color names currently don't match the colors.
#define C_YELLOW "\033[32;1m"
#define C_GREEN "\033[33;1m"
#define C_RED "\033[31;1m"
#define C_RESET "\033[0m"



/**
 * This only looks for type declarations
 */

namespace educ {
    DeclarationPointer getTypeDeclaration(QualifiedIdentifier id, DUContext* context, bool searchInParent=true);

}

