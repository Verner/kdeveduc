#include <grpc++/grpc++.h>

#include <language/backgroundparser/urlparselock.h>
#include <language/backgroundparser/parsejob.h>
#include <language/duchain/duchainlock.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/duchain.h>
#include <language/duchain/parsingenvironment.h>
#include <language/duchain/problem.h>
#include <language/duchain/builders/abstractdeclarationbuilder.h>
#include <language/duchain/builders/abstractcontextbuilder.h>
#include <language/duchain/builders/abstracttypebuilder.h>
#include <language/duchain/builders/abstractusebuilder.h>
#include <language/interfaces/ilanguagesupport.h>

#include <QTcpSocket>
#include <QReadLocker>
#include <QProcess>
#include <QDirIterator>

#include <debug.h>

#include "builder.h"
#include "kdevplugin.h"

#include "parsejob.h"
#include "rpcclient.h"

using namespace KDevelop;
using namespace ducproto;

educ::ParseJob::ParseJob(const KDevelop::IndexedString& url, KDevelop::ILanguageSupport* languageSupport, RPCClient* client)
 : KDevelop::ParseJob(url, languageSupport)
 , m_client(client)
{
}

const KDevelop::ParsingEnvironment* educ::ParseJob::environment() const
{
    return &env;
}


void educ::ParseJob::run(ThreadWeaver::JobPointer self, ThreadWeaver::Thread* thread)
{
   Q_UNUSED(self);
   Q_UNUSED(thread);
   UrlParseLock urlLock(document());

    if (! m_client) {
        qCDebug(PLUGIN_EDUC) << "No DUC Server for " << document();
        return;
    }

    qCDebug(PLUGIN_EDUC) << "Parsing" << document();

    if (abortRequested() ) { // || !isUpdateRequired(DUServerLang::languageString())
        qCDebug(PLUGIN_EDUC) << "No update required (or abort requested)";
        return;
    }

    // Try locking the editor revision
    if(readContents())
         return;

    // Get Editor Contents
    QByteArray code = contents().contents;

    // Remove trailing \0
    while(code.endsWith('\0'))
        code.chop(1);

    if(abortRequested())
      return;

    ParseResponse resp;

    qCDebug(PLUGIN_EDUC) << "Sending Parse Request to" << m_client->name();
    bool parse_ok = m_client->Parse(document().str(), code, &resp);
    qCDebug(PLUGIN_EDUC) << "Got Parse Response from" << m_client->name();

    ReferencedTopDUContext top_context = nullptr;

    if(parse_ok) // parse_result o.k.
    {
        QReadLocker parseLock(languageSupport()->parseLock());

        if(abortRequested())
            return abortJob();
        qCDebug(PLUGIN_EDUC) << "Building DU Chain from response" << m_client->name();
        top_context = buildTopDocumentContext(document(), &resp.root());
    } else {
        qCDebug(PLUGIN_EDUC) << "Parse request failed";
        return;
    }


    setDuChain(top_context);
    {
        DUChainWriteLocker lock;
//         context->setProblems(parse_result.problems());
        top_context->setFeatures(minimumFeatures());
        ParsingEnvironmentFilePointer file = top_context->parsingEnvironmentFile();
        Q_ASSERT(file);
        file->setModificationRevision(contents().modification);
        DUChain::self()->updateContextEnvironment(top_context, file.data());
    }
    highlightDUChain();

    DUChain::self()->emitUpdateReady(document(), duChain());
}
