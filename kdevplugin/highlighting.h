#pragma once

#include <language/highlighting/codehighlighting.h>

namespace educ {

    class Highlighting : public KDevelop::CodeHighlighting
    {
        Q_OBJECT
        public:
            Highlighting(QObject* parent);
            KDevelop::CodeHighlightingInstance* createInstance() const override;

    };

}
