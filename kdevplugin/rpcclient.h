#pragma once

#include <memory>

#include <grpc++/grpc++.h>

#include "duc.grpc.pb.h"

#include <QObject>
#include <QString>

class QProcess;
class QByteArray;

namespace educ {
    class RPCClient : public QObject {
        Q_OBJECT
    public:
        RPCClient(const QString& server_path);
        RPCClient(const int port);
        ~RPCClient();

        bool Parse (const QString& path, const QByteArray& contents, ducproto::ParseResponse* resp);
        bool Complete (const QString& path, const QByteArray& contents, const int cursor_pos, const int context_start, ducproto::CompletionResponse* resp);

        const QString name();

    private Q_SLOTS:
        void reStart();
        void onStdOut();

    private:
        void processLine();
        void kill();

        QString m_stdout;
        QString m_server_path;
        QProcess* m_server_proc;
        qint64 m_server_pid;
        int m_server_port;
        std::unique_ptr<ducproto::DUServer::Stub> m_client;
    };
}
