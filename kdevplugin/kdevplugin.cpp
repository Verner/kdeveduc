#include <debug.h>

#include <KPluginFactory>
#include <language/codecompletion/codecompletion.h>

#include "highlighting.h"
#include "parsejob.h"
#include "completionmodel.h"

#include "kdevplugin.h"

#include "rpcclient.h"

#include <KTextEditor/View>
#include <KTextEditor/ConfigInterface>

using namespace educ;

K_PLUGIN_FACTORY_WITH_JSON(educFactory, "kdevplugin.json", registerPlugin<EDUCPlugin>(); )

void educ::EDUCPlugin::startClients()
{
    m_clients["cs"] = new RPCClient("ducserver-csharp"); //     m_clients["cs"] = new DUClient(3569);
}


EDUCPlugin::EDUCPlugin(QObject *parent, const QVariantList& args)
    : KDevelop::IPlugin(QStringLiteral("educ"), parent)
    , KDevelop::ILanguageSupport()
    , m_highlighting(new Highlighting(this))
{
    Q_UNUSED(args);

    qCDebug(PLUGIN_EDUC) << "EDUC Plugin loaded!";
    startClients();
    KDevelop::CodeCompletionModel* codeCompletion = new DUServerCodeCompletionModel(this, this);
    auto model = new KDevelop::CodeCompletion(this, codeCompletion, name());

    connect(model, &KDevelop::CodeCompletion::registeredToView,
            this, [](KTextEditor::View* view){
                if (auto config = qobject_cast<KTextEditor::ConfigInterface*>(view)) {
                    config->setConfigValue(QStringLiteral("keyword-completion"), false);
                }
            }
    );
    connect(model, &KDevelop::CodeCompletion::unregisteredFromView,
            this, [](KTextEditor::View* view){
                if (auto config = qobject_cast<KTextEditor::ConfigInterface*>(view)) {
                    config->setConfigValue(QStringLiteral("keyword-completion"), true);
                }
            }
    );


//     PythonCodeCompletionModel* codeCompletion = new PythonCodeCompletionModel(this);
//     new KDevelop::CodeCompletion(this, codeCompletion, "Python");
//
//     auto assistantsManager = core()->languageController()->staticAssistantsManager();
//     assistantsManager->registerAssistant(StaticAssistant::Ptr(new RenameAssistant(this)));
//
//     QObject::connect(ICore::self()->documentController(), &IDocumentController::documentOpened,
//                      this, &LanguageSupport::documentOpened);

}



EDUCPlugin::~EDUCPlugin()
{
    for(auto client: m_clients.values())
        delete client;
}

QString EDUCPlugin::name() const
{
    return "EDUC";
}

RPCClient* EDUCPlugin::getClientForUrl(const KDevelop::IndexedString& url)
{
    QStringList url_parts = url.str().split(".");
    auto ext = url_parts[url_parts.length()-1];
    if (m_clients.contains(ext)) {
        return m_clients[ext];
    }
    return nullptr;
}


KDevelop::ParseJob* EDUCPlugin::createParseJob(const KDevelop::IndexedString& url)
{
    return new ParseJob(url, this, getClientForUrl(url));
}

KDevelop::ICodeHighlighting* EDUCPlugin::codeHighlighting() const
{
    return m_highlighting;
}


// needed for QObject class created from K_PLUGIN_FACTORY_WITH_JSON
#include "kdevplugin.moc"
