#pragma once

#include <interfaces/iplugin.h>
#include <language/interfaces/ilanguagesupport.h>

#include <QList>
#include <QString>

namespace KDevelop {
    class IndexedString;
    class ICodeHighlighting;
}

namespace educ {

    class Highlighting;
    class RPCClient;

    class EDUCPlugin : public KDevelop::IPlugin, public KDevelop::ILanguageSupport
    {
        Q_OBJECT
        Q_INTERFACES( KDevelop::ILanguageSupport )
    public:
        EDUCPlugin(QObject* parent, const QVariantList& args);

        ~EDUCPlugin() override;

        KDevelop::ParseJob* createParseJob(const KDevelop::IndexedString& url) override;
        QString name() const override;

        KDevelop::ICodeHighlighting* codeHighlighting() const override;

        RPCClient* getClientForUrl(const KDevelop::IndexedString& url);

    private:

        void startClients();

        QMap<QString, RPCClient*> m_clients;
        Highlighting* m_highlighting;
    };
}
