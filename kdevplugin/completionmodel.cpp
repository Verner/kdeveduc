#include "completionmodel.h"
#include <debug.h>


#include <language/codecompletion/codecompletionworker.h>
#include <language/duchain/topducontext.h>
#include <language/duchain/duchainutils.h>
#include <language/duchain/duchainlock.h>

#include <KTextEditor/View>
#include <KTextEditor/Document>

#include <QTimer>

#include "rpcclient.h"
#include "kdevplugin.h"

using namespace KDevelop;
using namespace educ;

namespace {

bool isSpaceOnly(const QString& string)
{
    return std::find_if(string.begin(), string.end(), [] (const QChar c) { return !c.isSpace(); }) == string.end();
}

int cursorToPos(const KTextEditor::Document* doc, const KTextEditor::Cursor& position) {
    int pos = 0, line = 0;
    while(line < position.line()) {
        pos += doc->lineLength(line)+1;
        line++;
    }
    return pos+position.column();
}

KTextEditor::Cursor posToCursor(const KTextEditor::Document* doc, const int pos) {
    int line_pos = 0, line = 0;
    while(line_pos+doc->lineLength(line) < pos)
        line_pos += doc->lineLength(line++);
    return KTextEditor::Cursor(line, pos-line_pos);
}

KTextEditor::Cursor posToCursor(const QString& doc, const int pos, const int start=0) {
    int column = 0, line = 0, read = start;
    while(read < pos) {
        if (doc[read] == "\n") {
            line++;
            column = 0;
        } else column++;
        read++;
    }

    return KTextEditor::Cursor(line, column);
}


class DUCompletionItem : public CompletionTreeItem
{
public:
    DUCompletionItem(const QString& display, const KTextEditor::Range& replacement_range, const QString& replacement_text, const KTextEditor::Cursor& cursor_placement, const QIcon& icon = QIcon())
        : CompletionTreeItem()
        , m_display(display)
        , m_replacement_range(replacement_range)
        , m_replacement_text(replacement_text)
        , m_cursor_placement(cursor_placement)
        , m_icon(icon)
        , m_unimportant(false)
    {
    }

    ~DUCompletionItem() override = default;

    void markAsUnimportant()
    {
        m_unimportant = true;
    }


    void execute(KTextEditor::View* view, const KTextEditor::Range&) override
    {
        m_replacement_range.setEnd(view->cursorPosition());
        view->document()->replaceText(m_replacement_range, m_replacement_text);
        view->setCursorPosition(view->cursorPosition());
    }

    QVariant data(const QModelIndex& index, int role, const CodeCompletionModel* model) const override
    {
        if (role == Qt::DecorationRole && index.column() == KTextEditor::CodeCompletionModel::Icon) {
            return m_icon;
        }
        if (role == CodeCompletionModel::UnimportantItemRole) {
            return m_unimportant;
        }

        if (role == Qt::DisplayRole) {
            if (index.column() == CodeCompletionModel::Prefix) {
                return "";
            } else if (index.column() == CodeCompletionModel::Name) {
                return m_display;
            }
        }
        return {};
    }

private:
    QString m_display;
    KTextEditor::Range m_replacement_range;
    QString m_replacement_text;
    KTextEditor::Cursor m_cursor_placement;
    QIcon m_icon;
    bool m_unimportant;
};


class DUServerCodeCompletionWorker : public CodeCompletionWorker
{
    Q_OBJECT
public:
    DUServerCodeCompletionWorker(CodeCompletionModel* model, EDUCPlugin* plugin)
        : CodeCompletionWorker(model)
        , m_plugin(plugin)
    {}
    ~DUServerCodeCompletionWorker() override = default;

public Q_SLOTS:
    void completionRequested(const QString& source, const QUrl &url, const int position, const int context_start)
    {
        // group requests and only handle the latest one
        m_url = url;
        m_position = position;
        m_source = source;
        m_context_start = context_start;

        if (!m_timer) {
            // lazy-load the timer to initialize it in the background thread
            m_timer = new QTimer(this);
            m_timer->setInterval(0);
            m_timer->setSingleShot(true);
            connect(m_timer, &QTimer::timeout, this, &DUServerCodeCompletionWorker::run);
        }
        m_timer->start();
    }

private:
    EDUCPlugin* m_plugin;


    void run()
    {
        aborting() = false;
        auto client = m_plugin->getClientForUrl(KDevelop::IndexedString(m_url.toLocalFile()));

        if (! client) {
            failed();
            return;
        }

        ducproto::CompletionResponse resp;

        bool parse_ok = client->Complete(m_url.toLocalFile(), m_source.toUtf8(), m_position, m_context_start, &resp);

        QList<QExplicitlySharedDataPointer<KDevelop::CompletionTreeItem>> completion_items;

        if (parse_ok && ! aborting()) // parse_result o.k.
        {
            for(auto const item: resp.items()) {
                KTextEditor::Cursor start = posToCursor(m_source, item.replace_start());
                KTextEditor::Cursor end = posToCursor(m_source, item.replace_end(), item.replace_start());
                end = KTextEditor::Cursor(start.line()+end.line(), start.line() < end.line() ? end.column() : start.column()+end.column());
                qCDebug(PLUGIN_EDUC) << "  " << QString::fromStdString(item.replace_text()) << "(" << start << "--" << end << ")";

                completion_items << QExplicitlySharedDataPointer<KDevelop::CompletionTreeItem>(new DUCompletionItem(
                    QString::fromStdString(item.replace_text()),
                    KTextEditor::Range(start, end),
                    QString::fromStdString(item.replace_text()),
                    end // posToCursor(m_source, item.cur_pos()) TODO: Figure out how to convert cur_pos to position after editing is done
                ));
            }

        } else {
            failed();
            return;
        }

        auto tree = computeGroups(completion_items, {} );

        if (aborting()) {
            failed();
            return;
        }

        foundDeclarations( tree, {} );
    }
private:
    QTimer* m_timer = nullptr;
    QUrl m_url;
    int m_position, m_context_start;
    QString m_source;
};
}

DUServerCodeCompletionModel::DUServerCodeCompletionModel(QObject* parent, EDUCPlugin* plugin)
    : CodeCompletionModel(parent)
    , m_plugin(plugin)
{
    qRegisterMetaType<KTextEditor::Cursor>();
}

DUServerCodeCompletionModel::~DUServerCodeCompletionModel() {};

bool DUServerCodeCompletionModel::shouldStartCompletion(KTextEditor::View* view, const QString& inserted,
                                                     bool userInsertion, const KTextEditor::Cursor& position)
{
    static const QString noCompletionAfter = QStringLiteral(";{}]) ");

    if (inserted.isEmpty() || isSpaceOnly(inserted)) {
        return false;
    }
    const auto lastChar = inserted.at(inserted.size() - 1);
    if (noCompletionAfter.contains(lastChar)) {
        return false;
    }

    if (userInsertion && inserted.endsWith(QStringLiteral("."))) {
        return true;
    }

    return KDevelop::CodeCompletionModel::shouldStartCompletion(view, inserted, userInsertion, position);
}


CodeCompletionWorker* DUServerCodeCompletionModel::createCompletionWorker()
{
    auto worker = new DUServerCodeCompletionWorker(this, m_plugin);
    connect(this, &DUServerCodeCompletionModel::requestCompletion,
            worker, &DUServerCodeCompletionWorker::completionRequested);
    return worker;
}



void DUServerCodeCompletionModel::completionInvokedInternal(KTextEditor::View* view, const KTextEditor::Range& range,
                                                         CodeCompletionModel::InvocationType /*invocationType*/, const QUrl &url)
{
    auto source = view->document()->text();
    auto pos = cursorToPos(view->document(), range.end());
    auto context_start = cursorToPos(view->document(), range.start());
    qCDebug(PLUGIN_EDUC) << "Completion at" << range << "(pos " << context_start <<"--"<< pos <<")";
    emit requestCompletion(source, url, pos, context_start);
}

#include "completionmodel.moc"
#include "moc_completionmodel.cpp"
