#pragma once

#include <language/backgroundparser/parsejob.h>
#include <language/duchain/parsingenvironment.h>


namespace educ {
    class RPCClient;

    class ParseJob : public KDevelop::ParseJob
    {
    public:
        ParseJob(const KDevelop::IndexedString& url, KDevelop::ILanguageSupport* languageSupport, RPCClient* client);

    protected:
        void run(ThreadWeaver::JobPointer self, ThreadWeaver::Thread* thread) override;

        const KDevelop::ParsingEnvironment* environment() const override;

    private:
        KDevelop::ParsingEnvironment env;
        RPCClient* m_client;
    };

}
