#pragma once

#include <language/codecompletion/codecompletionmodel.h>
#include <language/codecompletion/codecompletioncontext.h>

namespace educ
{
    class RPCClient;
    class EDUCPlugin;

    class DUServerCodeCompletionModel : public KDevelop::CodeCompletionModel
    {
        Q_OBJECT

    public:
        explicit DUServerCodeCompletionModel(QObject* parent, EDUCPlugin* plugin);
        ~DUServerCodeCompletionModel() override;

        bool shouldStartCompletion(KTextEditor::View* view, const QString& inserted,
                                bool userInsertion, const KTextEditor::Cursor& position) override;

    Q_SIGNALS:
        void requestCompletion(const QString& source, const QUrl &url, const int pos, const int context_start);

    protected:
        KDevelop::CodeCompletionWorker* createCompletionWorker() override;

        void completionInvokedInternal(KTextEditor::View* view, const KTextEditor::Range& range,
                                    InvocationType invocationType, const QUrl &url) override;

    public:
        EDUCPlugin* m_plugin;
    };


}
