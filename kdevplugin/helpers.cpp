#include <language/duchain/declaration.h>

#include "helpers.h"

using namespace KDevelop;


DeclarationPointer educ::getTypeDeclaration(QualifiedIdentifier id, DUContext* context, bool searchInParent)
{
    if(context)
    {
        auto declarations = context->findDeclarations(id, CursorInRevision(INT_MAX, INT_MAX));
        for(auto decl : declarations)
        {
            //TODO change this to just decl->kind() != Declaration::Type
            if((decl->kind() == Declaration::Import) || (decl->kind() == Declaration::Namespace)
                || (decl->kind() == Declaration::NamespaceAlias) || (decl->kind() == Declaration::Instance))
                continue;
            return DeclarationPointer(decl);
        }
        if(searchInParent && context->parentContext())
        {
            return educ::getTypeDeclaration(id, context->parentContext(), searchInParent);
        }
    }
    return DeclarationPointer();
}
